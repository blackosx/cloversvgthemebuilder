# CloverSvgThemeBuilder

# Overview

This script is designed to help theme designers generate a workable theme file for the Clover Boot Manager. The idea is to allow the designer to work on individual images and not worry about the required overall file structure which Clover expects. 

The designer can populate a Theme directory with:

* a settings.txt file
* a font SVG file and accompanying font_css.txt file
* the main individual .svg image files

then let the script generate a final theme.svg file for using with Clover.

**NEW** Symbols can now be used, though the sample theme does not currently contain these files. See below for some initial instructions.

## Dependencies


	└── Resources
			└── templateParts.sh


The templateParts.sh file contains the individual svg code structures that are used to build the final theme file.

## Theme directory

A structured Theme directory is required for the script to work with. If no theme is present, the script will give the option to download a sample theme to help demonstrate the script.

This sample theme includes Adobe Illustrator files as well as the SVG files to help designers get started with their themes.

## Theme directory structure

	Theme
	├── Font
	│   ├── font_css.txt
	│   └── <FONT>.svg
	├── Images
	│   ├── Banner.svg
	│   ├── Functions
	│   │   ├── func_about.svg
	│   │   ├── func_clover.svg
	│   │   ├── func_options.svg
	│   │   ├── func_reset.svg
	│   │   ├── func_shutdown.svg
	│   │   └── tool_shell.svg
	│   ├── FunctionsExtended
	│   │   ├── func_exit.svg
	│   │   ├── func_help.svg
	│   │   ├── func_secureboot.svg
	│   │   └── func_secureboot_config.svg
	│   ├── MenuControls
	│   │   ├── checkbox.svg
	│   │   ├── checkbox_checked.svg
	│   │   ├── radio_button.svg
	│   │   └── radio_button_selected.svg
	│   ├── OSBadges
	│   │   └── <OS FILES>.svg
	│   ├── Volumes
	│   │   ├── vol_clover.svg
	│   │   ├── vol_external.svg
	│   │   ├── vol_internal.svg
	│   │   ├── vol_internal_apfs.svg
	│   │   ├── vol_internal_ext3.svg
	│   │   ├── vol_internal_hfs.svg
	│   │   ├── vol_internal_ntfs.svg
	│   │   ├── vol_optical.svg
	│   │   └── vol_recovery.svg
	│   ├── background.svg
	│   ├── pointer.svg
	│   ├── selection_big.svg
	│   └── selection_small.svg
	└── settings.txt


### font_css.txt File

The file contains the css for describing the font to Clover.

Edit this to set the colour, font name and size you wish for your theme.

	.FontColour{fill:#FFFFFF;}
	.FontName{font-family:PTMono-Regular_Slim;}
	.FontSize{font-size:15px;}
	.FontSizeMenuTitle{font-size:18px;}
	.FontSizeMessageRow{font-size:22px;}
	.FontColourNight{fill:#7696D7;}
	.FontNameNight{font-family:PTMono-Regular;}
	.FontSizeNight{font-size:15px;}

### settings.txt File

The settings file contains the settings from Clover's Clovy theme.

Edit this to suit your theme.

	BackgroundDark="1"
	BackgroundScale="scale"
	BackgroundSharp="0x80"
	Badges="swap show"
	BadgeOffsetX="0x96"
	BadgeOffsetY="0x96"
	BadgeScale="0x04"
	BootCampStyle="0"
	CharWidth="12"
	NonSelectedGrey="0"
	SelectionColor="0x00252EFF"
	SelectionColor_night="0x0B1D37FF"
	SelectionOnTop="1"
	AnimeFrames=""
	FrameTime=""
	Version="0.87"
	Year="2018-2019"
	Author="Blackosx"
	Description="Vector theme (Based on Clovy theme file structure)"

## Symbols

Clover supports using symbols in your theme file. These can be described once but used many times. This script now has some support for working with symbols.

The current implementation works as follows:<br>
* A directory named SymbolFiles can be added to Theme/Images/ directory<br>
* That directory can then contain .svg symbols files and text files which describe their use.

	├── Theme
	│   ├── Font
	│   │   ├── PTMono-Regular_Latin+Cyrillic.svg
	│   │   └── font_css.txt
	│   ├── Images
	│   │   ├── Banner.svg
	│   │   ├── Banner_night.svg
	│   │   ├── SymbolFiles
	│   │   │   ├── MenuControls.ai
	│   │   │   ├── MenuControls.svg
	│   │   │   ├── symbols.svg
	│   │   │   ├── symbols_Checkbox_List.txt
	│   │   │   ├── symbols_FunctionsExtended_List.txt
	│   │   │   ├── symbols_Functions_List.txt
	│   │   │   ├── symbols_OSBadges_List.txt
	│   │   │   ├── symbols_RadioButton_List.txt
	│   │   │   ├── symbols_Scrollbar_List.txt
	│   │   │   └── symbols_volumes_List.txt
	│   │   ├── background.svg
	│   │   ├── background_night.svg
	│   │   ├── pointer.svg
	│   │   ├── selection_big.svg
	│   │   ├── selection_big_night.svg
	│   │   ├── selection_small.svg
	│   │   └── selection_small_night.svg
	│   └── settings.txt

In the above example two .svg files are being used:<br>
* MenuControls.svg, which contains images for ScrollBar Items, RadioButton Items and Checkbox Items.<br>
* symbols.svg, which contains images for Functions, FunctionsExtended, OSBadges and Volumes.

There are then various, pipe or vertical bar ( | ) delimited, .txt files which go on to describe how the symbols are to be used. Looking at *symbols\_Checkbox\_List.txt* we can see the following content:

	checkbox|Checkbox
	checkbox_checked|Checkbox|Tick
	checkbox_night|Checkbox|CheckboxNightOverlay
	checkbox_checked_night|Checkbox|Tick|CheckboxNightOverlay

Each line of the file starts with a group identifier that Clover expects to find in the theme file. Next is a pipe delimiter, before then going on to list symbol id's to use for that image.

To explain the above:

* Line 1 states, for the *checkbox* id in the theme file, build the image using the **Checkbox** symbol.

* Line 2 states, for the *checkbox\_checked* id, build the image using the **Checkbox** symbol AND the **Tick** symbol.

* Line 3 states, for the *checkbox\_night* id, build the image using the **Checkbox** symbol AND the **CheckboxNightOverlay** symbol.

* Line 4 states, for the *checkbox\_checked\_night* id, build the image using the **Checkbox** symbol AND the **Tick** symbol AND the **CheckboxNightOverlay** symbol.


<span style="color:red">Note, these text files have fixed names and should not be changed.</span>

## Usage

Clone the repository

```
git clone https://bitbucket.org/blackosx/cloversvgthemebuilder.git
```

Add your theme files, and double-click the CloverSvgThemeBuilder.command to execute the bash script.

### Switches

SHOW\_NIGHT\_ICONS=0

The line above (currently line #133 in the script) can be changes to 1 to have the theme preview show the night icons (if available in the theme files).

## Notes

* This has only been tested on SVG v1.1 files saved out from Adobe Illustrator.
* Each SVG file must have the content contained in a group.


## Links

* [Clover Scalable Themes Instructions](https://www.insanelymac.com/forum/topic/282787-clover-v2-instructions/?do=findComment&comment=2645125)
* [Clover Clovy Theme](https://sourceforge.net/p/cloverefiboot/code/HEAD/tree/CloverPackage/CloverV2/themespkg/Clovy/theme.svg)
* [Clovy's ExportCloverTheme script](https://bitbucket.org/Clovy_/exportclovertheme/src)

## License

* [GPL v3](https://opensource.org/licenses/GPL-3.0)

## Acknowledgments

* Clover Boot Manager
* Clover Clovy Theme

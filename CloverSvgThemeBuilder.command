#!/bin/bash

# A script to generate Clover theme svg template.
# Copyright (C) 2018-2019 Blackosx
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# v0.0.1 - 10th November 2018 - Initial script.
# v0.0.2 - 19th November 2018 - Change Merge script in to generator script
# v0.0.3 - 19th November 2018 - Expect some icons in their own directory
# v0.0.4 - 19th November 2018 - Add support for Menu Controls, Volumes and Functions
# v0.0.5 - 20th November 2018 - Append number to end of id's to make unique = FAIL
# v0.0.6 - 20th November 2018 - Ensure unique id's
# v0.0.7 - 21st November 2018 - Patch banner width and height
# v0.0.8 - 24th November 2018 - Add support for night background and moja icon
# v0.0.9 - 24th November 2018 - Separate clover settings file
# v0.1.0 - 26th November 2018 - Include FunctionsExtended and Patch OSBadge widths and height
# v0.1.1 - 26th November 2018 - Clean script and create extra functions
# v0.1.2 - 26th November 2018 - Create InsertItems function
# v0.1.3 - 28th November 2018 - Add scaling
# v0.1.4 - 29th November 2018 - Set width and height for control items
# v0.1.5 - 29th November 2018 - Rename Icons dir to Images. Get theme size from background
# v0.1.6 - 29th November 2018 - Font css to come from separate theme font_css.txt file 
# v0.1.7 - 29th November 2018 - Enable user to download sample theme, if wanted
# v0.1.8 - 30th November 2018 - Hide volumes from view if out of bounds
# v0.1.9 - 30th November 2018 - Improve placement of Selection images and warn if incorrectly sized.
# v0.2.0 -  3rd December 2018 - Show either day or night icons
# v0.2.1 -   8th January 2019 - Add initial support for Symbol files
# v0.2.2 -   9th January 2019 - Improve support for Symbol files
# v0.2.3 -   9th January 2019 - Add support for Multiple Symbols per icon
# v0.2.4 -   9th January 2019 - Fix showing night/day images in preview
# v0.2.5 -  10th January 2019 - Extend Menu Control support including symbols
# v0.2.6 -  11th January 2019 - Fix Night Menu
# v0.2.7 -  11th January 2019 - Scrollbar items now have dedicated directory
# v0.2.8 -  11th January 2019 - Allow two checkoxes, not just four
# v0.2.9 -  13th January 2019 - Include Selection Indicator
# v0.3.0 -  14th January 2019 - Add XposForCentred function
# v0.3.1 -  14th January 2019 - Restructure script to allow processing multiple theme(s)
# v0.3.2 -  14th January 2019 - Rework Logging
# v0.3.3 -  16th January 2019 - Fix badge positioning in preview
# v0.3.4 -  16th January 2019 - Allow random sorting of badges for preview
# v0.3.5 -  18th January 2019 - Add check for update
# v0.3.6 -  18th January 2019 - Fix update check
# v0.3.7 -  18th January 2019 - Add git pull option
# v0.3.8 -  20th January 2019 - Add global find and replace colour options to final theme file
# v0.3.9 -      20th May 2020 - Include Selection Indicator Night. Insert newlines between symbol layers
# v0.4.0 -      21st May 2020 - Attempt to adjust horizontal positioning of functions
# v0.4.1 -      22nd May 2020 - Add option to embed font
# v0.4.2 -      26th May 2020 - Hide selection_big when choosing to view night icons
# v0.4.3 -      27th May 2020 - Add option to choose whether badges/volumes outside of canvas are visible
# v0.4.4 -      28th May 2020 - Use var for final theme name
# v0.4.5 -     17th June 2020 - Ensure embedded font has ending </defs>

VER="0.4.5"

# ====================================
# SWITCHES
# ====================================

# Set to 1 to only show night icons / 0 to only show day icons
# This is purely visual for viewing the svg file
# This does not effect how Clover handles the theme
SHOW_NIGHT_ICONS=0

# Badges and Volumes when positioned off the canvas are set to hidden.
# Change this setting to 1 to leave them visible as it can be useful for
# wanting to check the quality of all the images at once by loading the
# file in to say Photoshop or another image editor.
HIDE_OUTISDE_CANVAS=1

# By default, the theme will default to showing the following badges in order:
# Windows, macOS 10.9, 10.10, 10.11, 10.12, 10.13, 10.14, 10.15
# Set to 1 to mix up badges shown in preview
# This does not effect how Clover handles the theme
RANDOMISE_BADGES=0

# The use of short SVG id's can make it difficult to identify individual elements
# later on if you wish to edit the theme file, or perform a global search/replace.
# The script with warn and list all id's less or equal to this length.
ID_LEN_WARN=3

# Set to 1 to embed font file in theme file
EMBED_FONT=1

# This will allow a theme to be set to a different size when viewing in an application
# for example, Adobe Illustrator. To use, change 1 to something like 1.5 or 2.
# WARNING. Leave set to 1 for a useable theme as Clover has it's own scaling method.
gSCALE=1

# Default placeholder text to use in the SVG file for preview only.
# Clover will change this when using the file.
MESSAGE_ROW_TEXT="Boot Windows"

#set -x

SELF_PATH=$(cd -P -- "$(dirname -- "$0")" && pwd -P) && SELF_PATH=$SELF_PATH/$(basename -- "$0")
SCRIPT_NAME=$(basename "$0")
SCRIPT_DIR="${SELF_PATH%/*}" && cd "$SCRIPT_DIR"

RESOURCES_DIR="$SCRIPT_DIR"/"Resources"
RESULTS_DIR="$SCRIPT_DIR"/"Results"
THEMES_DIR="$SCRIPT_DIR"/"Themes"
TMP_DIR="/tmp/CloverSVGThemeTmp"
TMP_DIR_UPDATE="/tmp/CloverSVGThemeUpdate"
TEMPLATE_PARTS="$RESOURCES_DIR"/templateParts.sh
FINAL_THEME_FILENAME="theme.svg"

SCRIPT_VER_CURRENT=$( echo "$VER" | tr -d . )
SCRIPT_VER_REPO=0

logScriptFileName="CloverSvgThemeBuilder.txt"
logScriptFile="$SCRIPT_DIR"/"$logScriptFileName"

logThemeFileName="Log.txt"
logThemeWarningFileName="Warnings.txt"

declare -a svgFiles
declare -a svgFilesOsBadges
declare -a svgFilesMenuControls
declare -a svgFilesVolumes
declare -a svgFilesFunctions
declare -a svgFilesFunctionsExtended
declare -a svgFilesScrollbar
declare -a svgFilesSymbols

declare -a arrSymbolsOsBadgeUsage
declare -a arrSymbolsFunctionsUsage
declare -a arrSymbolsFunctionsExtendedUsage
declare -a arrSymbolsVolumesUsage
declare -a arrSymbolsCheckboxItemsUsage
declare -a arrSymbolsRadioButtonItemsUsage

declare -a arrSymbolOsBadgeListItems
declare -a arrSymbolOsBadgeEntries
declare -a arrSymbolOsBadgeImageNames

declare -a arrSymbolFunctionsListItems
declare -a arrSymbolFunctionsEntries
declare -a arrSymbolFunctionsImageNames

declare -a arrSymbolFunctionsExtendedListItems
declare -a arrSymbolFunctionsExtendedEntries
declare -a arrSymbolFunctionsExtendedImageNames

declare -a arrSymbolVolumesListItems
declare -a arrSymbolVolumesEntries
declare -a arrSymbolVolumesImageNames

declare -a arrSymbolCheckboxListItems
declare -a arrSymbolCheckboxEntries
declare -a arrSymbolCheckboxImageNames

declare -a arrSymbolRadioButtonListItems
declare -a arrSymbolRadioButtonEntries
declare -a arrSymbolRadioButtonImageNames

declare -a arrSettings
declare -a arrFontCss
declare -a arrColourChanges

SYMBOL_FILE_BADGES_LIST="symbols_OSBadges_List.txt"
SYMBOL_FILE_FUNCTIONS_LIST="symbols_Functions_List.txt"
SYMBOL_FILE_FUNCTIONSEXTENDED_LIST="symbols_FunctionsExtended_List.txt"
SYMBOL_FILE_VOLUMES_LIST="symbols_volumes_List.txt"
SYMBOL_FILE_CHECKBOX_LIST="symbols_Checkbox_List.txt"
SYMBOL_FILE_RADIOBUTTON_LIST="symbols_RadioButton_List.txt"

doubleLines="===================================="
singleLine="-———————————————————————————————————"

# Set defaults

MESSAGE_ROW_XPOS=600

gTHEME_WIDTH=1200      # Default. Value will be overridden by background image width
gTHEME_HEIGHT=768      # Default. Value will be overridden by background image height

gTHEME_OSBADGE_Y=128
gTHEME_VOLUMES_Y=328
gTHEME_FUNCTIONS_Y=482
gTHEME_FUNCTIONSEXTENDED_Y=692
gTHEME_FUNCTION_SIZE=48

# ====================================
# FUNCTIONS
# ====================================

WriteToScriptLog()
{
 printf "%s\n" "${1}" >> "$logScriptFile"
}

# --------------------------------------------------

WriteToThemeLog()
{
 printf "%s\n" "${1}" >> "$logThemeFile"
}

# --------------------------------------------------

WriteToThemeWarningLog()
{
 printf "%s\n" "${1}" >> "$logThemeWarningFile"
}

# --------------------------------------------------

PrintToStdOut()
{
 printf "%s\n" "${1}"
}

# --------------------------------------------------

PrintToStdError()
{
 echo "${1}" >&2
}

# --------------------------------------------------

LogMultiple()
{
  # Function is being passed two arguments:
  # 0 - Log to write to
  # 1 - <Message>

  case "$1" in

    0 ) WriteToScriptLog "$2"
        PrintToStdOut "$2"
        ;;

    1 ) WriteToThemeLog "$2"
        PrintToStdOut "$2"
        ;;

    2 ) WriteToScriptLog "$2"
        WriteToThemeLog "$2" 
        PrintToStdOut "$2"
        ;;

    3 ) WriteToThemeWarningLog "$2"
        PrintToStdOut "$2"
        ;;

    4 ) WriteToScriptLog "$2"
        WriteToThemeWarningLog "$2"
        ;;

    5 ) WriteToThemeLog "$2"
        PrintToStdError "$2"
        ;;

    *) echo "Unknown logging ID with message $2" ;;

  esac
}

# --------------------------------------------------

PrintFileName()
{
  LogMultiple "2" "$singleLine"
  LogMultiple "2" "Working on: $1"
}

# --------------------------------------------------

ListFilesToLog()
{
  # Function is being passed:
  # 0 - name of directory
  # 1 - array of file names from directory

  local passedArray=("$@")

  WriteToThemeLog "$singleLine"
  LogMultiple "1" "${passedArray[0]}: Found $(( ${#passedArray[@]} -1 )) files to process"

  for (( a=1; a<${#passedArray[@]}; a++ ))
  do

    WriteToThemeLog "${passedArray[$a]}"

  done
}

# --------------------------------------------------

InsertSectionBreak()
{
  echo "" >> "$THEME_TEMPLATE"
  echo "<!-- $doubleLines -->" >> "$THEME_TEMPLATE"
  echo "<!-- $1 -->" >> "$THEME_TEMPLATE"
  echo "<!-- $doubleLines -->" >> "$THEME_TEMPLATE"
  echo "" >> "$THEME_TEMPLATE"
}

# --------------------------------------------------

InsertSubSectionBreak()
{
  echo "" >> "$THEME_TEMPLATE"
  echo "<!-- $singleLine -->" >> "$THEME_TEMPLATE"
  echo "<!-- $1 -->" >> "$THEME_TEMPLATE"
  echo "<!-- $singleLine -->" >> "$THEME_TEMPLATE"
  echo "" >> "$THEME_TEMPLATE"
}

# --------------------------------------------------

GetViewBox()
{
  local temp=$( grep viewBox "$1"/"$2".svg | head -n1 )
  temp="${temp##*viewBox=\"}"
  temp="${temp%%\"*}"
  echo "$temp"
}

# --------------------------------------------------

GetSvgViewBoxWidth()
{
  viewBox=$( GetViewBox "$1" "$2" )
  viewBoxWidth=$( echo "$viewBox" | cut -d' ' -f3 )
  if [ -z "$viewBoxWidth" ]; then
    echo 0
  else
    echo $viewBoxWidth
  fi
}

# --------------------------------------------------

GetSvgViewBoxHeight()
{
  viewBox=$( GetViewBox "$1" "$2" )
  viewBoxHeight=$( echo "$viewBox" | cut -d' ' -f4 )
  if [ -z "$viewBoxHeight" ]; then
    echo 0
  else
    echo $viewBoxHeight
  fi
}

# --------------------------------------------------

GetSvgViewBoxDimensionFromArray()
{
  # Function is being passed three arguments:
  # 0 - Width or Height
  # 1 - <PATH>
  # 2 - Array
  # passedArray will contain all arguments

  local passedArray=("$@")
  local value=0
  local returnValue=0

  for (( i=2; i<${#passedArray[@]}; i++ ))
  do

    [[ "${passedArray[0]}" == "Width" ]] && value=$( GetSvgViewBoxWidth "${passedArray[1]}" "${passedArray[$i]}" )
    [[ "${passedArray[0]}" == "Height" ]] && value=$( GetSvgViewBoxHeight "${passedArray[1]}" "${passedArray[$i]}" )

    if [ $value -ne $returnValue ]; then

      [[ $i -gt 2 ]] && echo "${passedArray[$i]} ${passedArray[0]} ($value) differs from last value ($returnValue)" >> "$logThemeWarningFile"

      returnValue=$value

    fi

  done
  
  echo $returnValue
}

# --------------------------------------------------

CalculateGap()
{
  local passedSelectionBoundBoxWidth=$1
  local passedIconBoundBoxWidth=$2

  if [ $passedSelectionBoundBoxWidth -gt $passedIconBoundBoxWidth ]; then

    selectionGap=$(( $passedSelectionBoundBoxWidth-$passedIconBoundBoxWidth ))
    Gap=$(( ($passedSelectionBoundBoxWidth-$passedIconBoundBoxWidth)/2 ))
    totalGap=$(( $selectionGap+$Gap ))

  else

    totalGap=16

  fi

  echo $totalGap
}

# --------------------------------------------------

GetAndPatchSvgFileContent()
{
  declare -a arrSvgFile
  declare -a arrSvgFileIds
  local returnString

  # Extract icon part from file

  sed -n -e '/<g/,$p' "$1"/"$2".svg > "$TMP_DIR"/"$2".svg
  sed \$d "$TMP_DIR"/"$2".svg > "$TMP_DIR"/"$2"_.svg

  if [ -f "$TMP_DIR"/"$2".svg ] && [ -f "$TMP_DIR"/"$2"_.svg ]; then

    # Read file in to array and identify id's

    OIFS=$IFS; IFS=$'\n\r';

    arrSvgFile=($(<"${TMP_DIR}/$2_.svg"))

    # Loop through array to find entries with ids

    if [ ${#arrSvgFile[@]} -gt 0 ]; then

      for (( f=0; f<${#arrSvgFile[@]}; f++ ))
      do

        if [[ "${arrSvgFile[$f]}" == *" id=\""* ]]; then

         numIdsInLine=$( echo "${arrSvgFile[$f]}" | grep -o " id=\"" | wc -l )

         # Strip id and append to array

         idStart=$( echo "${arrSvgFile[$f]#*id=\"}" )

         for (( n=0; n<$numIdsInLine; n++ ))
         do

           idComplete=$( echo "${idStart%%\"*}" )
           idStart=$( echo "${arrSvgFile[$f]#*id=\"$idComplete\" }" )
           idStart=$( echo "${idStart#*id=\"}" )
           arrSvgFileIds+=("${idComplete}")

         done

        fi

      done

      # Now have individual id's in array
      # DEBUG

      WriteToThemeLog ""
      WriteToThemeLog "-----------------------------"
      WriteToThemeLog "File: $2.svg"
      WriteToThemeLog "Number of id's in file = ${#arrSvgFileIds[@]}"

      for (( f=0; f<${#arrSvgFileIds[@]}; f++ ))
      do

        WriteToThemeLog "[$3][$f] ${arrSvgFileIds[$f]}"

        if [ ${#arrSvgFileIds[$f]} -le $ID_LEN_WARN ]; then

          LogMultiple "4" "*** Warning | $2.svg : Short id [${arrSvgFileIds[$f]}] may cause global replace issue!"

        fi

      done

      # Loop through id's, updating file array with all instances of id with extra unique number

      for (( p=0; p<${#arrSvgFileIds[@]}; p++ ))
      do

        for (( f=0; f<${#arrSvgFile[@]}; f++ ))
        do

          if [[ "${arrSvgFile[$f]}" == *"${arrSvgFileIds[$p]}"* ]]; then

            arrSvgFile[$f]=$( echo "${arrSvgFile[$f]}" | sed "s/${arrSvgFileIds[$p]}/${arrSvgFileIds[$p]}[$3]/g" )

          fi

        done

      done

      # Build final string to return

      for (( f=0; f<${#arrSvgFile[@]}; f++ ))
      do

        returnString="$returnString${arrSvgFile[$f]}"

      done

    fi

    IFS=$OIFS

    # Escape all ampersands

    returnString=$( echo "$returnString" | sed 's/&/\\\&/g' )

    # Escape forward slashes

    returnString=$( echo "$returnString" | sed 's/\//\\\//g' )

  fi

  echo "$returnString"
}

# --------------------------------------------------

InsertItems()
{
  # Function is being passed seven arguments:
  # 0 - Unique ID to use
  # 1 - Item Type
  # 2 - X Position
  # 3 - Bounding Box Width
  # 4 - Bounding Box Height
  # 5 - Gap
  # 6 - Array of file names from directory
  # passedArray will contain all arguments

  local passedArray=("$@")

  local passedUniqueId="${passedArray[0]}"
  local passedType="${passedArray[1]}"
  local passedX="${passedArray[2]}"
  local passedBoundingBoxWidth="${passedArray[3]}"
  local passedBoundingBoxHeight="${passedArray[4]}"
  local passedGap="${passedArray[5]}"
  local tmpSection=""
  local nightX=$passedX

  if [ "$passedType" == "Badge" ]; then

    local path="$IMAGES_DIR"/"$OSBADGES_DIR_NAME"
    local template="$templateOSBadge"

  elif [ "$passedType" == "Volume" ]; then

    local path="$IMAGES_DIR"/"$VOLUMES_DIR_NAME"
    local template="$templateVolume"

  elif [ "$passedType" == "Function" ]; then

    local path="$IMAGES_DIR"/"$FUNCTIONS_DIR_NAME"
    local template="$templateFunction"

  elif [ "$passedType" == "FunctionExtended" ]; then

    local path="$IMAGES_DIR"/"$FUNCTIONSEXTENDED_DIR_NAME"
    local template="$templateFunctionExtended"

  elif [ "$passedType" == "CheckBox" ]; then

    local path="$IMAGES_DIR"/"$MENUCONTROLS_DIR_NAME"
    local template="$templateCheckbox"

  elif [ "$passedType" == "RadioButton" ]; then

    local path="$IMAGES_DIR"/"$MENUCONTROLS_DIR_NAME"
    local template="$templateRadioButton"

  fi

  for (( d=6; d<${#passedArray[@]}; d++ ))
  do

    if [ "$passedType" == "CheckBox" ] && [[ "${passedArray[$d]}" != "checkbox"* ]]; then
      continue
    fi

    if [ "$passedType" == "RadioButton" ] && [[ "${passedArray[$d]}" != "radio_button"* ]]; then
      continue
    fi

    #PrintFileName "${passedArray[$d]}"

    tmpSvgData=$( GetAndPatchSvgFileContent "$path" "${passedArray[$d]}" $passedUniqueId )

    ((passedUniqueId++))

    tmpSection=$( echo "$template" | sed "s/<!--INSERT-FILENAME-->/${passedArray[$d]}/g" )

    if [[ "${passedArray[$d]}" != *"_night" ]]; then   # this is a day icon

      tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-X-->/${passedX}/g" )

    else                                               # this is a night icon

      tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-X-->/${nightX}/g" )

    fi

    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-WIDTH-->/${passedBoundingBoxWidth}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-HEIGHT-->/${passedBoundingBoxHeight}/g" )

    # Hide icon from viewing theme file if outside theme width

    if [ "$passedType" == "Badge" ] || [ "$passedType" == "Volume" ]; then

      if [ $SHOW_NIGHT_ICONS -eq 0 ]; then # Show day icons

        if [ $(( ($passedBoundingBoxWidth+$passedGap)+$passedX)) -gt $gTHEME_WIDTH ]; then
          tmpSection=$( echo "$tmpSection" | sed "s/DisplayInline/DisplayNone/g" )
        fi

      else

        if [ $(( ($passedBoundingBoxWidth+$passedGap)+$nightX)) -gt $gTHEME_WIDTH ]; then
          tmpSection=$( echo "$tmpSection" | sed "s/DisplayInline/DisplayNone/g" )
        fi
  
      fi

    fi


    # Show or Hide 'night' items

    if [[ "${passedArray[$d]}" != *"_night" ]]; then   # this is a day icon

      if [ $SHOW_NIGHT_ICONS -eq 1 ]; then             # Setting is to show night icons

        # Set icon to hidden

        tmpSection=$( echo "$tmpSection" | sed "s/DisplayInline/DisplayNone/g" ) && LogMultiple "5" "Setting ${passedArray[$d]} as hidden"

      fi

      LogMultiple "5" "Inserting $passedType: ${passedArray[$d]} W($passedBoundingBoxWidth)xH($passedBoundingBoxHeight) at X($passedX)"

      passedX=$(( $passedX+$passedBoundingBoxWidth+$passedGap ))

    else                                               # this is a night icon

      if [ $SHOW_NIGHT_ICONS -eq 0 ]; then             # Setting is to show day icons

        # Set icon to hidden

        tmpSection=$( echo "$tmpSection" | sed "s/DisplayInline/DisplayNone/g" ) && LogMultiple "5" "Setting ${passedArray[$d]} as hidden"

      fi

      LogMultiple "5" "Inserting $passedType: ${passedArray[$d]} W($passedBoundingBoxWidth)xH($passedBoundingBoxHeight) at X($nightX)"

      nightX=$(( $nightX+$passedBoundingBoxWidth+$passedGap ))

    fi
 
    # Insert SVG image data

    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILECONTENT-->/${tmpSvgData}/g" )

    # Insert in to Theme template

    echo "$tmpSection" >> "$THEME_TEMPLATE"

  done
  
  echo $passedUniqueId
}

# --------------------------------------------------

InsertSymbolItems()
{
  # Function is being passed seven arguments:
  # 0 - Unique ID to use
  # 1 - Item Type
  # 2 - X Position
  # 3 - Bounding Box Width
  # 4 - Bounding Box Height
  # 5 - Gap
  # 6 - Array of file names from directory
  # passedArray will contain all arguments

  local passedArray=("$@")

  local passedUniqueId="${passedArray[0]}"
  local passedType="${passedArray[1]}"
  local passedX="${passedArray[2]}"
  local passedBoundingBoxWidth="${passedArray[3]}"
  local passedBoundingBoxHeight="${passedArray[4]}"
  local passedGap="${passedArray[5]}"
  local tmpSection=""
  local nightX=$passedX

  unset arrUsage
  unset arrImageNames

  if [ "$passedType" == "Badge" ]; then

    local template="$templateOSBadge"

    # Copy arrays to temporary arrays

    arrUsage=("${arrSymbolsOsBadgeUsage[@]}") 
    arrImageNames=("${arrSymbolOsBadgeImageNames[@]}") 

  elif [ "$passedType" == "Volume" ]; then

    local template="$templateVolume"
    arrUsage=("${arrSymbolsVolumesUsage[@]}") 
    arrImageNames=("${arrSymbolVolumesImageNames[@]}") 

  elif [ "$passedType" == "Function" ]; then

    local template="$templateFunction"
    arrUsage=("${arrSymbolsFunctionsUsage[@]}") 
    arrImageNames=("${arrSymbolFunctionsImageNames[@]}") 

  elif [ "$passedType" == "FunctionExtended" ]; then

    local template="$templateFunctionExtended"
    arrUsage=("${arrSymbolsFunctionsExtendedUsage[@]}") 
    arrImageNames=("${arrSymbolFunctionsExtendedImageNames[@]}") 

  elif [ "$passedType" == "CheckBox" ]; then

    local template="$templateCheckbox"
    arrUsage=("${arrSymbolsCheckboxItemsUsage[@]}") 
    arrImageNames=("${arrSymbolCheckboxImageNames[@]}") 

  elif [ "$passedType" == "RadioButton" ]; then

    local template="$templateRadioButton"
    arrUsage=("${arrSymbolsRadioButtonItemsUsage[@]}") 
    arrImageNames=("${arrSymbolRadioButtonImageNames[@]}") 

  fi

  for (( d=6; d<${#passedArray[@]}; d++ ))
  do

    tmpSection=$( echo "$template" | sed "s/<!--INSERT-FILENAME-->/${passedArray[$d]}/g" )

    if [[ "${passedArray[$d]}" != *"_night" ]]; then   # this is a day icon

      tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-X-->/${passedX}/g" )

    else                                               # this is a night icon

      tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-X-->/${nightX}/g" )

    fi

    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-WIDTH-->/${passedBoundingBoxWidth}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-HEIGHT-->/${passedBoundingBoxHeight}/g" )

    # Hide icon from viewing theme file if outside theme width

    if [ "$passedType" == "Badge" ] || [ "$passedType" == "Volume" ]; then

      if [ $HIDE_OUTISDE_CANVAS -eq 1 ]; then

        if [ $SHOW_NIGHT_ICONS -eq 0 ]; then # Show day icons

          if [ $(( ($passedBoundingBoxWidth+$passedGap)+$passedX)) -gt $gTHEME_WIDTH ]; then
            tmpSection=$( echo "$tmpSection" | sed "s/DisplayInline/DisplayNone/g" )
          fi

        else

          if [ $(( ($passedBoundingBoxWidth+$passedGap)+$nightX)) -gt $gTHEME_WIDTH ]; then
            tmpSection=$( echo "$tmpSection" | sed "s/DisplayInline/DisplayNone/g" )
          fi

        fi

      fi

    fi

    # Show or Hide 'night' items depending on Setting, and night icons exist in theme

    if [[ "${passedArray[$d]}" != *"_night" ]]; then   # this is a day icon

      if [ $SHOW_NIGHT_ICONS -eq 1 ]; then             # Setting is to show night icons

        # Set icon to hidden

        tmpSection=$( echo "$tmpSection" | sed "s/DisplayInline/DisplayNone/g" ) && LogMultiple "5" "Setting ${passedArray[$d]} as hidden"

      fi

      LogMultiple "5" "Inserting $passedType: ${passedArray[$d]} W($passedBoundingBoxWidth)xH($passedBoundingBoxHeight) at X($passedX)"

      passedX=$(( $passedX+$passedBoundingBoxWidth+$passedGap ))

    else                                               # this is a night icon

      if [ $SHOW_NIGHT_ICONS -eq 0 ]; then             # Setting is to show day icons

        # Set icon to hidden

        tmpSection=$( echo "$tmpSection" | sed "s/DisplayInline/DisplayNone/g" ) && LogMultiple "5" "Setting ${passedArray[$d]} as hidden"

      fi

      LogMultiple "5" "Inserting $passedType: ${passedArray[$d]} W($passedBoundingBoxWidth)xH($passedBoundingBoxHeight) at X($nightX)"

      nightX=$(( $nightX+$passedBoundingBoxWidth+$passedGap ))

    fi

    # Match FileContent with Symbol Usage

    unset array
    IFS='|' read -r -a array <<< "${arrImageNames[$((d-6))]}"

    tmpLinesToInsert=""

    for (( i=0; i<${#array[@]}; i++ ))
    do

      for (( p=0; p<${#arrUsage[@]}; p++ ))
      do

        if [[ "${arrUsage[$p]}" == *#${array[$i]}\"* ]]; then

          LogMultiple "5" "Inserting Symbol: ${array[$i]}"

          # For readability of the file, add an @@~~ to the end of this string to represent a newline
          # These are then globally replaced at the end with a newline

          tmpLinesToInsert="$tmpLinesToInsert${arrUsage[$p]}@@~~"

          break

        fi

      done

    done

    # Escape all ampersands

    tmp=$( echo "$tmpLinesToInsert" | sed 's/&/\\\&/g' )

    # Escape forward slashes

    tmp=$( echo "$tmp" | sed 's/\//\\\//g' )

    # Patch temp section with Os Badge Usage Line

    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILECONTENT-->/$tmp/g" )

    # Insert in to Theme template

    echo "$tmpSection" >> "$THEME_TEMPLATE"

  done

  echo $passedUniqueId
}

# --------------------------------------------------

ApplyScale()
{
  local tmpHeader="$1"

  if [ $gSCALE != "1" ]; then

    LogMultiple "1" "Applying Scaling of $gSCALE:"
    local tmpHeader=$( echo "$tmpHeader" | sed "s/scale(1)/scale($gSCALE)/g" )

  fi

  echo "$tmpHeader"
}

# --------------------------------------------------

NightImagesInArray()
{
  local passedArray=("$@")
  local nightImageCount=0

  for (( c=0; c<${#svgFilesFunctions[@]}; c++ ))
  do

    if [[ "${passedArray[$c]}" == *"_night" ]]; then
      ((nightImageCount++))
    fi

  done

  echo $nightImageCount
}

# --------------------------------------------------

XposForCentred()
{
  x=$1

  if [ ! -z "$1" ]; then

    local x=$(echo "scale=0;($gTHEME_WIDTH-($1))/2" | bc)

  fi

  echo "$x"
}


# --------------------------------------------------

CheckForUpdate()
{

  # Was script directory downloaded or cloned from git?

  if [ -d "$SCRIPT_DIR"/.git ]; then

    WriteToScriptLog "<Checking for any updates to the git repo....>"

    # From https://stackoverflow.com/a/3278427

    git remote update &>/dev/null

    UPSTREAM=${1:-'@{u}'}
    LOCAL=$(git rev-parse @)
    REMOTE=$(git rev-parse "$UPSTREAM")
    BASE=$(git merge-base @ "$UPSTREAM")

    if [ $LOCAL = $REMOTE ]; then
      touch "$TMP_DIR_UPDATE"/gitNoChange && WriteToScriptLog "<Update Check:Up-to-date>"
    elif [ $LOCAL = $BASE ]; then
      touch "$TMP_DIR_UPDATE"/gitPull && WriteToScriptLog "<Update Check:Need to pull>"
    elif [ $REMOTE = $BASE ]; then
      touch "$TMP_DIR_UPDATE"/gitPush && WriteToScriptLog "<Update Check:Need to push>"
    else
      touch "$TMP_DIR_UPDATE"/gitDiverged && WriteToScriptLog "<Update Check:Diverged>"
    fi

  else

    WriteToScriptLog "<Checking for any updates to this script....>"

		local lastCommit=$(curl -Ls https://bitbucket.org/blackosx/cloversvgthemebuilder/src | grep "data-current-cset=")

		if [ ! -z "$lastCommit" ]; then

			lastCommit="${lastCommit#*\"}"
			lastCommit="${lastCommit%\"*}"

			if [ "${#lastCommit}" -eq 40 ]; then

				WriteToScriptLog "<Last Commit=$lastCommit>"

				curl -s https://bitbucket.org/blackosx/cloversvgthemebuilder/raw/${lastCommit}/CloverSvgThemeBuilder.command?raw > "$TMP_DIR_UPDATE"/source

			fi

		fi

  fi
}

# --------------------------------------------------

ReplaceThemeColours()
{

  # Read Colour Changes file

  PrintFileName "${COLOUR_CHANGE_FILE##*/}"

  local line
  local search
  local replace

  OIFS=$IFS; IFS=$'\n\r';

  arrColourChanges=($(<"${COLOUR_CHANGE_FILE}"))

  for (( f=0; f<${#arrColourChanges[@]}; f++ ))
  do

    line="${arrColourChanges[$f]}"

    if [ "${line:0:3}" != "@@@" ]; then

      LogMultiple "1" "$line"

      # Split List Items by |

      search="${line%%|*}"
      replace="${line#*|}"

      if [ ! -z "$search" ] && [ ! -z "$replace" ]; then

        LogMultiple "1" "search:$search | replace:$replace"

        sed -i -e "s/${search}/${replace}/g" "$TMP_DIR"/"$FINAL_THEME_FILENAME"

      else

        LogMultiple "1" "search/replace contains a blank"

      fi

    fi

  done

  IFS=$OIFS

}

# --------------------------------------------------

ProcessTheme()
{

  local themeDir="$1"

  LogMultiple "0" ""
  LogMultiple "0" $doubleLines	
  LogMultiple "0" "Processing Theme ${themeDir##*/}"
  LogMultiple "0" $doubleLines
  LogMultiple "0" ""

  THEME_DIR="$themeDir"
  IMAGES_DIR_NAME="Images"
  IMAGES_DIR="$THEME_DIR"/"$IMAGES_DIR_NAME"
  FONT_DIR="$THEME_DIR"/"Font"

  OSBADGES_DIR_NAME="OSBadges"
  MENUCONTROLS_DIR_NAME="MenuControls"
  VOLUMES_DIR_NAME="Volumes"
  FUNCTIONS_DIR_NAME="Functions"
  FUNCTIONSEXTENDED_DIR_NAME="FunctionsExtended"
  SCROLLBAR_DIR_NAME="Scrollbar"
  SYMBOLS_DIR_NAME="SymbolFiles"

  SETTINGS_FILE="$THEME_DIR"/settings.txt
  COLOUR_CHANGE_FILE="$THEME_DIR"/colour_changes.txt
  THEME_TEMPLATE="$TMP_DIR"/"$FINAL_THEME_FILENAME"
  FONT_CSS_FILE="$FONT_DIR"/font_css.txt
  SYMBOLS_FILENAME="symbols.svg"
  SYMBOLS_FILE="$IMAGES_DIR"/"$SYMBOLS_DIR_NAME"/"$SYMBOLS_FILENAME"

  if [ ! -f "$SETTINGS_FILE" ]; then
      LogMultiple "0" "Theme Settings file is missing!"
      LogMultiple "0" "Exiting."
      exit 1
  fi

  if [ ! -d "$FONT_DIR" ]; then
      LogMultiple "0" "$FONT_DIR Directory is missing!"
      LogMultiple "0" "Exiting."
      exit 1
  elif [ ! -f "$FONT_CSS_FILE" ]; then
      LogMultiple "0" "Theme font_css file is missing!"
      LogMultiple "0" "Exiting."
      exit 1
  fi

  if [ ! -d "$IMAGES_DIR" ]; then
      LogMultiple "0" "$IMAGES_DIR Directory is missing!"
      LogMultiple "0" "Exiting."
      exit 1
  fi

  RESULT_DIR="$RESULTS_DIR"/"${themeDir##*/}"
  mkdir "$RESULT_DIR"

  logThemeWarningFile="$RESULT_DIR"/"$logThemeWarningFileName"
  logThemeFile="$RESULT_DIR"/"$logThemeFileName"

  unset svgFiles
  unset svgFilesOsBadges
  unset svgFilesMenuControls
  unset svgFilesVolumes
  unset svgFilesFunctions
  unset svgFilesFunctionsExtended
  unset svgFilesScrollbar
  unset svgFilesSymbols

  unset arrSymbolsOsBadgeUsage
  unset arrSymbolsFunctionsUsage
  unset arrSymbolsFunctionsExtendedUsage
  unset arrSymbolsVolumesUsage
  unset arrSymbolsCheckboxItemsUsage
  unset arrSymbolsRadioButtonItemsUsage

  unset arrSymbolOsBadgeListItems
  unset arrSymbolOsBadgeEntries
  unset arrSymbolOsBadgeImageNames

  unset arrSymbolFunctionsListItems
  unset arrSymbolFunctionsEntries
  unset arrSymbolFunctionsImageNames

  unset arrSymbolFunctionsExtendedListItems
  unset arrSymbolFunctionsExtendedEntries
  unset arrSymbolFunctionsExtendedImageNames

  unset arrSymbolVolumesListItems
  unset arrSymbolVolumesEntries
  unset arrSymbolVolumesImageNames

  unset arrSymbolCheckboxListItems
  unset arrSymbolCheckboxEntries
  unset arrSymbolCheckboxImageNames

  unset arrSymbolRadioButtonListItems
  unset arrSymbolRadioButtonEntries
  unset arrSymbolRadioButtonImageNames

  unset arrSettings
  unset arrFontCss
  unset arrColourChanges

  gUniqueId=0
  gTotalRootSvgFiles=0

  SYMBOL_FILE_BADGES=0
  SYMBOL_FILE_FUNCTIONS=0
  SYMBOL_FILE_FUNCTIONSEXTENDED=0
  SYMBOL_FILE_VOLUMES=0
  SYMBOL_FILE_CHECKBOX=0
  SYMBOL_FILE_RADIOBUTTON=0

  gTHEME_SELECTIONBIG_BOUNDINGBOX_WIDTH=0
  gTHEME_SELECTIONBIG_BOUNDINGBOX_HEIGHT=0

  gTHEME_SELECTIONSMALL_BOUNDINGBOX_WIDTH=0
  gTHEME_SELECTIONSMALL_BOUNDINGBOX_HEIGHT=0

  gTHEME_BADGE_SWAP_STATE="none"

  # --------------------------------------------------

  # Build list of SVG files to process

  OIFS=$IFS; IFS=$'\n\r'

  if [ -d "$IMAGES_DIR" ]; then

    pushd "$IMAGES_DIR" > /dev/null

    svgFiles=($(ls *.svg | cut -d. -f1))

    if [ -d "$OSBADGES_DIR_NAME" ]; then
      pushd "$OSBADGES_DIR_NAME" > /dev/null

      if [ $RANDOMISE_BADGES -eq 1 ]; then

        LogMultiple "2" "Random sorting badge list"
        svgFilesOsBadges=($(ls *.svg | cut -d. -f1 | sort --random-sort ))

      else

        svgFilesOsBadges=($(ls *.svg | cut -d. -f1 ))

      fi

      popd > /dev/null
    fi

    if [ -d "$MENUCONTROLS_DIR_NAME" ]; then
      pushd "$MENUCONTROLS_DIR_NAME" > /dev/null
      svgFilesMenuControls=($(ls *.svg | cut -d. -f1 ))
      popd > /dev/null
    fi

    if [ -d "$VOLUMES_DIR_NAME" ]; then
      pushd "$VOLUMES_DIR_NAME" > /dev/null
      svgFilesVolumes=($(ls *.svg | cut -d. -f1 ))
      popd > /dev/null
    fi

    if [ -d "$FUNCTIONS_DIR_NAME" ]; then
      pushd "$FUNCTIONS_DIR_NAME" > /dev/null
      svgFilesFunctions=($(ls *.svg | cut -d. -f1 ))
      popd > /dev/null
    fi

    if [ -d "$FUNCTIONSEXTENDED_DIR_NAME" ]; then
      pushd "$FUNCTIONSEXTENDED_DIR_NAME" > /dev/null
      svgFilesFunctionsExtended=($(ls *.svg | cut -d. -f1 ))
      popd > /dev/null
    fi

    if [ -d "$SCROLLBAR_DIR_NAME" ]; then
      pushd "$SCROLLBAR_DIR_NAME" > /dev/null
      svgFilesScrollbar=($(ls *.svg | cut -d. -f1 ))
      popd > /dev/null
    fi

    if [ -d "$SYMBOLS_DIR_NAME" ]; then
      pushd "$SYMBOLS_DIR_NAME" > /dev/null
      svgFilesSymbols=($(ls *.svg | cut -d. -f1 ))
      popd > /dev/null
    fi

  fi

  IFS=$OIFS

  # Check there are some SVG files present

  gTotalRootSvgFiles=${#svgFiles[@]}

  ListFilesToLog "$IMAGES_DIR_NAME" "${svgFiles[@]}"
  ListFilesToLog "$OSBADGES_DIR_NAME" "${svgFilesOsBadges[@]}"
  ListFilesToLog "$MENUCONTROLS_DIR_NAME" "${svgFilesMenuControls[@]}"
  ListFilesToLog "$VOLUMES_DIR_NAME" "${svgFilesVolumes[@]}"
  ListFilesToLog "$FUNCTIONS_DIR_NAME" "${svgFilesFunctions[@]}"
  ListFilesToLog "$FUNCTIONSEXTENDED_DIR_NAME" "${svgFilesFunctionsExtended[@]}"
  ListFilesToLog "$SYMBOLS_DIR_NAME" "${svgFilesSymbols[@]}"

  WriteToThemeLog "$singleLine"

  # --------------------------------------------------

  source "$TEMPLATE_PARTS"

  LogMultiple "1" "Theme scale set to: $gSCALE"

  # Begin constructing template, insert document header

  echo "$templateDocumentHeader" >> "$THEME_TEMPLATE"

  # --------------------------------------------------

  # Get Theme Size from Background image

  backgroundImage=""

  if [ -f "$IMAGES_DIR"/"background.svg" ]; then

    backgroundImage="background"

  elif [ -f "$IMAGES_DIR"/"background_night.svg" ]; then

    backgroundImage="background_night"

  fi

  if [ "$backgroundImage" != "" ]; then

    gTHEME_WIDTH=$( GetSvgViewBoxWidth "$IMAGES_DIR" "$backgroundImage" )
    gTHEME_HEIGHT=$( GetSvgViewBoxHeight "$IMAGES_DIR" "$backgroundImage" )

  else

    LogMultiple "1" "Background image missing. Unknown Theme Size. Using defaults."

  fi

  LogMultiple "1" "Background Dimensions: W=$gTHEME_WIDTH | H=$gTHEME_HEIGHT"

  # Apply Scale to SVG Header ViewBox

  # Note:
  # Only patch the SVG Header.
  # Leave global Theme Width & Height set as per artwork files.

  scaled_THEME_WIDTH=$( echo "scale=2; $gTHEME_WIDTH*$gSCALE" | bc)
  scaled_THEME_HEIGHT=$( echo "scale=2; $gTHEME_HEIGHT*$gSCALE" | bc)

  if [ $gSCALE != "1" ]; then

    LogMultiple "1" "Theme Size after Scaling: W=$scaled_THEME_WIDTH | H=$scaled_THEME_HEIGHT"

  fi

  tmpSection="$templateSvgHeader"
  tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-WIDTH-->/${scaled_THEME_WIDTH}/g" )
  tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-HEIGHT-->/${scaled_THEME_HEIGHT}/g" )

  echo "$tmpSection" >> "$THEME_TEMPLATE"

  # -----------------------------------

  # Read Clover Theme Settings

  PrintFileName "${SETTINGS_FILE##*/}"

  OIFS=$IFS; IFS=$'\n\r';

  arrSettings=($(<"${SETTINGS_FILE}"))

  echo "<clover:theme" >> "$THEME_TEMPLATE"

  for (( f=0; f<${#arrSettings[@]}; f++ ))
  do

    LogMultiple "1" "${arrSettings[$f]}"

    if [[ "${arrSettings[$f]}" == *"Badges="* ]]; then

      if [[ "${arrSettings[$f]}" == *swap* ]]; then

        gTHEME_BADGE_SWAP_STATE="swap"

      fi

    fi

    #Insert Clover Theme Header Settings

    echo "${arrSettings[$f]}" >> "$THEME_TEMPLATE"

  done

  IFS=$OIFS

  echo "/>" >> "$THEME_TEMPLATE"

  # --------------------------------------------------

  # Set Vertical position of Badges and Volumes

  if [ "$gTHEME_BADGE_SWAP_STATE" == "none" ]; then

    gTHEME_OSBADGE_Y=128
    gTHEME_VOLUMES_Y=328

  elif [ "$gTHEME_BADGE_SWAP_STATE" == "swap" ]; then

    gTHEME_OSBADGE_Y=328
    gTHEME_VOLUMES_Y=128

  else

    # TO DO
    gTHEME_OSBADGE_Y=128
    gTHEME_VOLUMES_Y=128

  fi

  # --------------------------------------------------

  # Get Selection Big size

  if [ -f "$IMAGES_DIR"/selection_big.svg ]; then

    gTHEME_SELECTIONBIG_BOUNDINGBOX_WIDTH=$( GetSvgViewBoxWidth "$IMAGES_DIR" "selection_big" )
    gTHEME_SELECTIONBIG_BOUNDINGBOX_HEIGHT=$( GetSvgViewBoxHeight "$IMAGES_DIR" "selection_big" )

  fi

  if [ $gTHEME_SELECTIONBIG_BOUNDINGBOX_WIDTH -ne 144 ] || [ $gTHEME_SELECTIONBIG_BOUNDINGBOX_HEIGHT -ne 144 ]; then

    LogMultiple "3" "Warning. selection_big.svg is $gTHEME_SELECTIONBIG_BOUNDINGBOX_WIDTH x $gTHEME_SELECTIONBIG_BOUNDINGBOX_HEIGHT. Should be 144x144."

  else

    LogMultiple "1" "selection_big.svg size: $gTHEME_SELECTIONBIG_BOUNDINGBOX_WIDTH x $gTHEME_SELECTIONBIG_BOUNDINGBOX_HEIGHT"

  fi

  # Get Selection Small size

  if [ -f "$IMAGES_DIR"/selection_small.svg ]; then

    gTHEME_SELECTIONSMALL_BOUNDINGBOX_WIDTH=$( GetSvgViewBoxWidth "$IMAGES_DIR" "selection_small" )
    gTHEME_SELECTIONSMALL_BOUNDINGBOX_HEIGHT=$( GetSvgViewBoxHeight "$IMAGES_DIR" "selection_small" )

  fi

  if [ $gTHEME_SELECTIONSMALL_BOUNDINGBOX_WIDTH -ne 64 ] || [ $gTHEME_SELECTIONSMALL_BOUNDINGBOX_HEIGHT -ne 64 ]; then

    LogMultiple "3" "Warning. selection_small.svg is $gTHEME_SELECTIONSMALL_BOUNDINGBOX_WIDTH x $gTHEME_SELECTIONSMALL_BOUNDINGBOX_HEIGHT. Should be 64x64."

  else

    LogMultiple "1" "selection_small.svg size: $gTHEME_SELECTIONSMALL_BOUNDINGBOX_WIDTH x $gTHEME_SELECTIONSMALL_BOUNDINGBOX_HEIGHT"

  fi

  # --------------------------------------------------

  echo "$templateCSS" >> "$THEME_TEMPLATE"

  # Patch font CSS in to template CSS

  # Read Theme font CSS file

  PrintFileName "${FONT_CSS_FILE##*/}"

  OIFS=$IFS; IFS=$'\n\r';

  arrFontCss=($(<"${FONT_CSS_FILE}"))

  for (( f=0; f<${#arrFontCss[@]}; f++ ))
  do

    LogMultiple "1" "${arrFontCss[$f]}"

    #Insert Clover Theme Header Settings

    echo "${arrFontCss[$f]}" >> "$THEME_TEMPLATE"

  done

  IFS=$OIFS

  echo "$templateCSSClose" >> "$THEME_TEMPLATE"

  # --------------------------------------------------

  if [ $EMBED_FONT -eq 1 ]; then

    tmpFontData=""

    for svgFontFiles in "$FONT_DIR"/*.svg ;
    do

      fontName=$(basename "$svgFontFiles")
      LogMultiple "2" "Embedding font: $fontName"

      numLinesInFile=$( wc -l "$svgFontFiles" | awk '{print $1}' )

      metaDataLineNo=$( grep -n "<metadata>" "$svgFontFiles" )
      metaDataLineNo="${metaDataLineNo%%:*}"

      if [ -n "$metaDataLineNo" ]; then

        if [ $metaDataLineNo -lt $numLinesInFile ]; then

          fontContent=$(sed '$d' "$svgFontFiles" | tail -n +$metaDataLineNo)
          tmpFontData="${tmpFontData}${fontContent}"

          # Check for closing </defs>
          lastLineinFile=$(tail -n1 "$svgFontFiles")
          if [ "$lastLineinFile" == "</defs></svg>" ]; then
            tmpFontData="${tmpFontData}@@~~</defs>@@~~"
          fi

        else

          LogMultiple "2" "XXX"

        fi

      else

        LogMultiple "2" "Failed to find <metadata> tag in font file"

      fi

    done

    echo "$tmpFontData" >> "$THEME_TEMPLATE"

  else

    echo "$templateFont" >> "$THEME_TEMPLATE"

    # Move any font files to RESULTS

    for svgFontFiles in "$FONT_DIR"/*.svg ;
    do

      cp "$svgFontFiles" "$RESULT_DIR"

    done
 
  fi

  # --------------------------------------------------

  # Read Symbols file and insert Symbols

  if [ ${#svgFilesSymbols[@]} -gt 0 ]; then

    for (( d=0; d<${#svgFilesSymbols[@]}; d++ ))
    do

      InsertSectionBreak "SYMBOLS [${svgFilesSymbols[$d]}]"

      PrintFileName "${svgFilesSymbols[$d]}".svg

      # Extract Symbols and insert in to theme file

      sed -n -e '/<symbol/,/<\/symbol>/p' "$IMAGES_DIR"/"$SYMBOLS_DIR_NAME"/"${svgFilesSymbols[$d]}".svg >> "$THEME_TEMPLATE"

      # Find line of file with last </symbol> entry

      lastClosingSymbolLineNumber=$(grep -n "</symbol>" "$IMAGES_DIR"/"$SYMBOLS_DIR_NAME"/"${svgFilesSymbols[$d]}".svg | tail -1)
      lastClosingSymbolLineNumber="${lastClosingSymbolLineNumber%%:*}"
      ((lastClosingSymbolLineNumber++))

      # Write Remainder of File to temp file 

      tail -n +$lastClosingSymbolLineNumber "$IMAGES_DIR"/"$SYMBOLS_DIR_NAME"/"${svgFilesSymbols[$d]}".svg > "$TMP_DIR"/SymbolRemainder.txt

      # Process SymbolRemainder.txt file

      # Remove blank lines

      awk 'NF' "$TMP_DIR"/SymbolRemainder.txt > "$TMP_DIR"/SymbolRemainderClean.txt

      OIFS=$IFS; IFS=$'\n\r';

      for i in $( grep "<g id=\"" "$TMP_DIR"/SymbolRemainderClean.txt );do

        LogMultiple "1" "Extracting Symbol Usage: $i"

        strippedId="${i#*\"}"
        strippedId="${strippedId%\"*}"

        sed "/^$i/,/<\/g/!d" "$TMP_DIR"/SymbolRemainderClean.txt > "$TMP_DIR"/SymbolUsage_"$strippedId".txt

        # Remove first and last lines
        sed -ie '1d' "$TMP_DIR"/SymbolUsage_"$strippedId".txt
        sed -ie '$d' "$TMP_DIR"/SymbolUsage_"$strippedId".txt

        # Populate arrays

        case "$strippedId" in

          "OSBadges" )            arrSymbolsOsBadgeUsage=($(<"$TMP_DIR"/SymbolUsage_"$strippedId".txt))
                                  SYMBOL_FILE_BADGES=1

                                  if [ $RANDOMISE_BADGES -eq 1 ]; then

                                    LogMultiple "2" "Random sorting badge list"

                                    sort --random-sort "${IMAGES_DIR}/${SYMBOLS_DIR_NAME}/${SYMBOL_FILE_BADGES_LIST}" > "${TMP_DIR}/OsBadgeListSorted"
                                    arrSymbolOsBadgeListItems=($(<"${TMP_DIR}/OsBadgeListSorted"))

                                  else

                                    arrSymbolOsBadgeListItems=($(<"$IMAGES_DIR"/"$SYMBOLS_DIR_NAME"/"$SYMBOL_FILE_BADGES_LIST"))

                                  fi

                                  # Split List Items by |

                                  for (( s=0; s<${#arrSymbolOsBadgeListItems[@]}; s++ ))
                                  do

                                    arrSymbolOsBadgeEntries+=( "${arrSymbolOsBadgeListItems[$s]%%|*}" )
                                    arrSymbolOsBadgeImageNames+=( "${arrSymbolOsBadgeListItems[$s]#*|}" )

                                  done
                                  ;;

          "Functions")            arrSymbolsFunctionsUsage=($(<"$TMP_DIR"/SymbolUsage_"$strippedId".txt))
                                  SYMBOL_FILE_FUNCTIONS=1

                                  arrSymbolFunctionsListItems=($(<"$IMAGES_DIR"/"$SYMBOLS_DIR_NAME"/"$SYMBOL_FILE_FUNCTIONS_LIST"))

                                  # Split List Items by |

                                  for (( s=0; s<${#arrSymbolFunctionsListItems[@]}; s++ ))
                                  do

                                    arrSymbolFunctionsEntries+=( "${arrSymbolFunctionsListItems[$s]%%|*}" )
                                    arrSymbolFunctionsImageNames+=( "${arrSymbolFunctionsListItems[$s]#*|}" )

                                  done
                                  ;;

          "FunctionsExtended")    arrSymbolsFunctionsExtendedUsage=($(<"$TMP_DIR"/SymbolUsage_"$strippedId".txt))
                                  SYMBOL_FILE_FUNCTIONSEXTENDED=1

                                  arrSymbolFunctionsExtendedListItems=($(<"$IMAGES_DIR"/"$SYMBOLS_DIR_NAME"/"$SYMBOL_FILE_FUNCTIONSEXTENDED_LIST"))

                                  # Split List Items by |

                                  for (( s=0; s<${#arrSymbolFunctionsExtendedListItems[@]}; s++ ))
                                  do

                                    arrSymbolFunctionsExtendedEntries+=( "${arrSymbolFunctionsExtendedListItems[$s]%%|*}" )
                                    arrSymbolFunctionsExtendedImageNames+=( "${arrSymbolFunctionsExtendedListItems[$s]#*|}" )

                                  done
                                  ;;

          "Volumes")              arrSymbolsVolumesUsage=($(<"$TMP_DIR"/SymbolUsage_"$strippedId".txt))
                                  SYMBOL_FILE_VOLUMES=1

                                  arrSymbolVolumesListItems=($(<"$IMAGES_DIR"/"$SYMBOLS_DIR_NAME"/"$SYMBOL_FILE_VOLUMES_LIST"))

                                  # Split List Items by |

                                  for (( s=0; s<${#arrSymbolVolumesListItems[@]}; s++ ))
                                  do

                                    arrSymbolVolumesEntries+=( "${arrSymbolVolumesListItems[$s]%%|*}" )
                                    arrSymbolVolumesImageNames+=( "${arrSymbolVolumesListItems[$s]#*|}" )

                                  done
                                  ;;

          "Checkbox_Items")       arrSymbolsCheckboxItemsUsage=($(<"$TMP_DIR"/SymbolUsage_"$strippedId".txt))
                                  SYMBOL_FILE_CHECKBOX=1

                                  arrSymbolCheckboxListItems=($(<"$IMAGES_DIR"/"$SYMBOLS_DIR_NAME"/"$SYMBOL_FILE_CHECKBOX_LIST"))

                                  # Split List Items by |

                                  for (( s=0; s<${#arrSymbolCheckboxListItems[@]}; s++ ))
                                  do

                                    arrSymbolCheckboxEntries+=( "${arrSymbolCheckboxListItems[$s]%%|*}" )
                                    arrSymbolCheckboxImageNames+=( "${arrSymbolCheckboxListItems[$s]#*|}" )

                                  done
                                  ;;

          "RadioButton_Items")    arrSymbolsRadioButtonItemsUsage=($(<"$TMP_DIR"/SymbolUsage_"$strippedId".txt))
                                  SYMBOL_FILE_RADIOBUTTON=1

                                  arrSymbolRadioButtonListItems=($(<"$IMAGES_DIR"/"$SYMBOLS_DIR_NAME"/"$SYMBOL_FILE_RADIOBUTTON_LIST"))

                                  # Split List Items by |

                                  for (( s=0; s<${#arrSymbolRadioButtonListItems[@]}; s++ ))
                                  do

                                    arrSymbolRadioButtonEntries+=( "${arrSymbolRadioButtonListItems[$s]%%|*}" )
                                    arrSymbolRadioButtonImageNames+=( "${arrSymbolRadioButtonListItems[$s]#*|}" )

                                  done
                                  ;;

          *) LogMultiple "1" "Unknown Symbol ID found"
             ;;

        esac

      done

    done

    IFS=$OIFS

  fi

  # --------------------------------------------------

  # Insert Background

  idx=0
  while [ "${svgFiles[$idx]}" != "background" ] && [ $idx -le $gTotalRootSvgFiles ]
  do
    ((idx++))
  done

  if [ $idx -le ${#svgFiles[@]} ] && [ "${svgFiles[$idx]}" == "background" ]; then

    PrintFileName "${svgFiles[$idx]}"

    InsertSectionBreak "BACKGROUND"

    tmpSvgData=$( GetAndPatchSvgFileContent "$IMAGES_DIR" "${svgFiles[$idx]}" $gUniqueId )

    ((gUniqueId++))

    tmpSection=$( ApplyScale "$templateBackground" )

    if [ $SHOW_NIGHT_ICONS -eq 1 ]; then

      tmpSection=$( echo "$tmpSection" | sed "s/transform/class=\"DisplayNone\" transform/g" )

    fi

    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILECONTENT-->/${tmpSvgData}/g" )

    # Insert in to Theme template

    echo "$tmpSection" >> "$THEME_TEMPLATE"

  fi

  # --------------------------------------------------

  # Insert Background Night

  idx=0
  while [ "${svgFiles[$idx]}" != "background_night" ] && [ $idx -le $gTotalRootSvgFiles ]
  do
    ((idx++))
  done

  if [ $idx -le ${#svgFiles[@]} ] && [ "${svgFiles[idx]}" == "background_night" ]; then

    PrintFileName "${svgFiles[$idx]}"

    InsertSectionBreak "BACKGROUND NIGHT"

    tmpSvgData=$( GetAndPatchSvgFileContent "$IMAGES_DIR" "${svgFiles[$idx]}" $gUniqueId )

    ((gUniqueId++))

    tmpSection=$( ApplyScale "$templateBackgroundNight" )

    if [ $SHOW_NIGHT_ICONS -eq 0 ]; then

      tmpSection=$( echo "$tmpSection" | sed "s/transform/class=\"DisplayNone\" transform/g" )

    fi

    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILECONTENT-->/${tmpSvgData}/g" )

    # Insert in to Theme template

    echo "$tmpSection" >> "$THEME_TEMPLATE"

  fi

  InsertSectionBreak "MENU SCREEN"

  tmpSection=$( ApplyScale "$templateHeaderMenuScreen" )
  echo "$tmpSection" >> "$THEME_TEMPLATE"

  echo "$templateHeaderMenuContainer" >> "$THEME_TEMPLATE"
  echo "$templateContainerBorder" >> "$THEME_TEMPLATE"
  echo "$templateMenuSelection" >> "$THEME_TEMPLATE"
  echo "$templateMenuRows" >> "$THEME_TEMPLATE"
  echo "$templateMenuSelectionNight" >> "$THEME_TEMPLATE"
  echo "$templateMenuRowsNight" >> "$THEME_TEMPLATE"
  echo "$templateCloseGroup" >> "$THEME_TEMPLATE"

  InsertSectionBreak "MENU CONTROLS"

  echo "$templateHeaderControls" >> "$THEME_TEMPLATE"

  # --------------------------------------------------

  # Insert Menu Controls icons

  # Scrollbar

  PrintFileName "Scrollbar"

  if [ ${#svgFilesScrollbar[@]} -eq 4 ] || [ ${#svgFilesScrollbar[@]} -eq 2 ]; then

    echo "$templateHeaderMenuScroll" >> "$THEME_TEMPLATE"

    for (( s=0; s<${#svgFilesScrollbar[@]}; s++ ))
    do

      if [[ "${svgFilesScrollbar[$s]}" != *_night ]]; then

        LogMultiple "1" "${svgFilesScrollbar[$s]}"

        tmpSvgData=$( GetAndPatchSvgFileContent "${IMAGES_DIR}/${SCROLLBAR_DIR_NAME}" "${svgFilesScrollbar[$s]}" $gUniqueId )

        ((gUniqueId++))

        tmpSection=$( ApplyScale "$templateScrollBar" )

        tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILENAME-->/${svgFilesScrollbar[$s]}/g" )
        tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILECONTENT-->/${tmpSvgData}/g" )

        # Insert in to Theme template

        echo "$tmpSection" >> "$THEME_TEMPLATE"

      fi

    done

    echo "$templateCloseGroup" >> "$THEME_TEMPLATE"
    echo "$templateHeaderMenuScrollNight" >> "$THEME_TEMPLATE"

    for (( s=0; s<${#svgFilesScrollbar[@]}; s++ ))
    do

      if [[ "${svgFilesScrollbar[$s]}" == *_night ]]; then

        LogMultiple "1" "${svgFilesScrollbar[$s]}"

        tmpSvgData=$( GetAndPatchSvgFileContent "${IMAGES_DIR}/${SCROLLBAR_DIR_NAME}" "${svgFilesScrollbar[$s]}" $gUniqueId )

        ((gUniqueId++))

        tmpSection=$( ApplyScale "$templateScrollBar" )

        tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILENAME-->/${svgFilesScrollbar[$s]}/g" )
        tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILECONTENT-->/${tmpSvgData}/g" )

        # Insert in to Theme template

        echo "$tmpSection" >> "$THEME_TEMPLATE"

      fi

    done

  fi

  # Checkboxes

  PrintFileName "Checkbox"

  if [ $SYMBOL_FILE_CHECKBOX -eq 0 ]; then

    controlItemWidth=$( GetSvgViewBoxWidth "$IMAGES_DIR"/"$MENUCONTROLS_DIR_NAME" "${svgFilesMenuControls[$d]}" )
    controlItemHeight=$( GetSvgViewBoxHeight "$IMAGES_DIR"/"$MENUCONTROLS_DIR_NAME" "${svgFilesMenuControls[$d]}" )

  else

    controlItemWidth=16
    controlItemHeight=16

  fi

  echo "$templateCloseGroup" >> "$THEME_TEMPLATE"

  if [ $SYMBOL_FILE_CHECKBOX -eq 0 ]; then

    gUniqueId=$( InsertItems "$gUniqueId" "CheckBox" "0" "$controlItemWidth" "$controlItemHeight" "0" "${svgFilesMenuControls[@]}" )

  else

    gUniqueId=$( InsertSymbolItems "$gUniqueId" "CheckBox" "0" "$controlItemWidth" "$controlItemHeight" "0" "${arrSymbolCheckboxEntries[@]}" )

  fi

  # RadioButtons

  PrintFileName "RadioButton"

  if [ $SYMBOL_FILE_RADIOBUTTON -eq 0 ]; then

    gUniqueId=$( InsertItems "$gUniqueId" "RadioButton" "0" "$controlItemWidth" "$controlItemHeight" "0" "${svgFilesMenuControls[@]}" )

  else

    gUniqueId=$( InsertSymbolItems "$gUniqueId" "RadioButton" "0" "$controlItemWidth" "$controlItemHeight" "0" "${arrSymbolRadioButtonEntries[@]}" )

  fi

  echo "$templateCloseGroup" >> "$THEME_TEMPLATE"

  echo "$templateMenuTitle" >> "$THEME_TEMPLATE"
  echo "$templateMenuIcon" >> "$THEME_TEMPLATE"
  echo "$templateCloseGroup" >> "$THEME_TEMPLATE"

  InsertSectionBreak "MAIN SCREEN"

  echo "$templateHeaderMainScreen" >> "$THEME_TEMPLATE"

  # --------------------------------------------------

  # Insert Volumes

  # Patch Volumes Y placement

  PrintFileName "Volumes"

  tmpSection=$( ApplyScale "$templateHeaderVolumes" )

  tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-Y-->/${gTHEME_VOLUMES_Y}/g" )

  # Set visibility depending on Badges setting.

  if [ "$gTHEME_BADGE_SWAP_STATE" == "swap" ]; then

    tmpSection=$( echo "$tmpSection" | sed "s/transform/class=\"DisplayNone\" transform/g" )

  fi

  echo "$tmpSection" >> "$THEME_TEMPLATE"

  LogMultiple "1" "---------------------------"
  LogMultiple "1" "Checking Width and Height of Volumes"

  if [ $SYMBOL_FILE_VOLUMES -eq 0 ]; then

    boundingBoxVolumeWidth=$( GetSvgViewBoxDimensionFromArray "Width" "$IMAGES_DIR"/"$VOLUMES_DIR_NAME" "${svgFilesVolumes[@]}" )
    boundingBoxVolumeHeight=$( GetSvgViewBoxDimensionFromArray "Height" "$IMAGES_DIR"/"$VOLUMES_DIR_NAME" "${svgFilesVolumes[@]}" )

  else

    symbolsfileExtensionRemoved="${SYMBOLS_FILENAME%*.svg}"
    boundingBoxVolumeWidth=$( GetSvgViewBoxWidth "$IMAGES_DIR"/"$SYMBOLS_DIR_NAME" "$symbolsfileExtensionRemoved" )
    boundingBoxVolumeHeight=$( GetSvgViewBoxHeight "$IMAGES_DIR"/"$SYMBOLS_DIR_NAME" "$symbolsfileExtensionRemoved" )

  fi

  LogMultiple "1" "Global Volume Bounding Box width: $boundingBoxVolumeWidth"
  LogMultiple "1" "Global Volume Bounding Box Height: $boundingBoxVolumeHeight"

  # Calculate X placement for Volumes
  # Note. This is only for satisfaction of viewing theme file. Clover doesn't care about this placement.

  if [ $SYMBOL_FILE_VOLUMES -eq 0 ]; then

    nightImageCount=$( NightImagesInArray "${svgFilesVolumes[@]}" )
    dayImageCount=$(( ${#svgFilesVolumes[@]}-$nightImageCount ))

  else

    # Count number of Icons from List file
    nightImageCount=$( grep "_night" "$IMAGES_DIR"/"$SYMBOLS_DIR_NAME"/"$SYMBOL_FILE_VOLUMES_LIST" | wc -l )
    nightImageCount=$( echo "$nightImageCount" | tr -d '[:space:]' )

    dayImageCount=$( cat "$IMAGES_DIR"/"$SYMBOLS_DIR_NAME"/"$SYMBOL_FILE_VOLUMES_LIST" | wc -l )
    dayImageCount=$( echo "$dayImageCount" | tr -d '[:space:]' )
    dayImageCount=$(( dayImageCount - nightImageCount ))

  fi

  totalGap=$( CalculateGap "$gTHEME_SELECTIONBIG_BOUNDINGBOX_WIDTH" "$boundingBoxVolumeWidth" )

  numberOfVolumesThatFit=$(( $gTHEME_WIDTH/($boundingBoxVolumeWidth+$totalGap) ))

  if [ $SHOW_NIGHT_ICONS -eq 1 ]; then
    [[ $nightImageCount -lt $numberOfVolumesThatFit ]] && numberOfVolumesThatFit=$nightImageCount
  fi

  if [ $SHOW_NIGHT_ICONS -eq 0 ]; then
    [[ $dayImageCount -lt $numberOfVolumesThatFit ]] && numberOfVolumesThatFit=$dayImageCount
  fi

  volumeAreaWidth=$(( ($boundingBoxVolumeWidth+$totalGap)*$numberOfVolumesThatFit ))
  volumeAreaWidth=$(( $volumeAreaWidth - $totalGap ))
  volumeAreaX=$( XposForCentred "$volumeAreaWidth" ) 

  if [ $SYMBOL_FILE_VOLUMES -eq 0 ]; then

    gUniqueId=$( InsertItems "$gUniqueId" "Volume" "$volumeAreaX" "$boundingBoxVolumeWidth" "$boundingBoxVolumeHeight" "$totalGap" "${svgFilesVolumes[@]}" )

  else

    gUniqueId=$( InsertSymbolItems "$gUniqueId" "Volume" "$volumeAreaX" "$boundingBoxVolumeWidth" "$boundingBoxVolumeHeight" "$totalGap" "${arrSymbolVolumesEntries[@]}" )

  fi

  echo "$templateCloseGroup" >> "$THEME_TEMPLATE"

  # --------------------------------------------------

  # Insert Function Extended icons

  #if [ ${#svgFilesFunctionsExtended[@]} -gt 0 ]; then

    # Patch Functions Y placement

    InsertSubSectionBreak "MAIN SCREEN - FunctionsExtended"

    tmpSection=$( ApplyScale "$templateHeaderFunctionsExtended" )

    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-Y-->/${gTHEME_FUNCTIONSEXTENDED_Y}/g" )

    echo "$tmpSection" >> "$THEME_TEMPLATE"

    LogMultiple "1" "---------------------------"
    LogMultiple "1" "Checking Width and Height of Functions Extended"

    if [ $SYMBOL_FILE_FUNCTIONSEXTENDED -eq 0 ]; then

      boundingBoxFuncExtendedWidth=$( GetSvgViewBoxDimensionFromArray "Width" "$IMAGES_DIR"/"$FUNCTIONSEXTENDED_DIR_NAME" "${svgFilesFunctionsExtended[@]}" )
      boundingBoxFuncExtendedHeight=$( GetSvgViewBoxDimensionFromArray "Height" "$IMAGES_DIR"/"$FUNCTIONSEXTENDED_DIR_NAME" "${svgFilesFunctionsExtended[@]}" )

    else

      #symbolsfileExtensionRemoved="${SYMBOLS_FILENAME%*.svg}"
      #boundingBoxFuncExtendedWidth=$( GetSvgViewBoxWidth "$IMAGES_DIR"/"$SYMBOLS_DIR_NAME" "$symbolsfileExtensionRemoved" )
      #boundingBoxFuncExtendedHeight=$( GetSvgViewBoxHeight "$IMAGES_DIR"/"$SYMBOLS_DIR_NAME" "$symbolsfileExtensionRemoved" )

      boundingBoxFuncExtendedWidth=$gTHEME_FUNCTION_SIZE
      boundingBoxFuncExtendedHeight=$gTHEME_FUNCTION_SIZE

    fi

    LogMultiple "1" "Global Functions Extended Bounding Box width: $boundingBoxFuncExtendedWidth"
    LogMultiple "1" "Global Functions Extended Bounding Box Height: $boundingBoxFuncExtendedHeight"

    # Calculate X placement for Functions
    # Note. This is only for satisfaction of viewing theme file. Clover doesn't care about this placement.

    if [ $SYMBOL_FILE_FUNCTIONSEXTENDED -eq 0 ]; then

      nightImageCount=$( NightImagesInArray "${svgFilesFunctionsExtended[@]}" )
      dayImageCount=$(( ${#svgFilesFunctionsExtended[@]}-$nightImageCount ))

    else

      # Count number of Icons from List file
      nightImageCount=$( grep "_night" "$IMAGES_DIR"/"$SYMBOLS_DIR_NAME"/"$SYMBOL_FILE_FUNCTIONSEXTENDED_LIST" | wc -l )
      nightImageCount=$( echo "$nightImageCount" | tr -d '[:space:]' )

      dayImageCount=$( cat "$IMAGES_DIR"/"$SYMBOLS_DIR_NAME"/"$SYMBOL_FILE_FUNCTIONSEXTENDED_LIST" | wc -l )
      dayImageCount=$( echo "$dayImageCount" | tr -d '[:space:]' )
      dayImageCount=$(( dayImageCount - nightImageCount ))

    fi

    totalGap=$( CalculateGap "$gTHEME_SELECTIONSMALL_BOUNDINGBOX_WIDTH" "$boundingBoxFuncExtendedWidth" )

    numberOfFunctionsExtendedThatFit=$(( $gTHEME_WIDTH/($boundingBoxFuncExtendedWidth+$totalGap) ))

    if [ $SHOW_NIGHT_ICONS -eq 1 ]; then
      [[ $nightImageCount -lt $numberOfFunctionsExtendedThatFit ]] && numberOfFunctionsExtendedThatFit=$nightImageCount
    fi

    if [ $SHOW_NIGHT_ICONS -eq 0 ]; then
      [[ $dayImageCount -lt $numberOfFunctionsExtendedThatFit ]] && numberOfFunctionsExtendedThatFit=$dayImageCount
    fi

    FunctionsExtendedAreaWidth=$(( ($boundingBoxFuncExtendedWidth+$totalGap)*$numberOfFunctionsExtendedThatFit ))
    FunctionsExtendedAreaWidth=$(( $FunctionsExtendedAreaWidth - $totalGap ))
    #FunctionsExtendedAreaX=$((( $gTHEME_WIDTH-$FunctionsExtendedAreaWidth)/2))
    FunctionsExtendedAreaX=$( XposForCentred "$FunctionsExtendedAreaWidth" ) 

    if [ $SYMBOL_FILE_FUNCTIONSEXTENDED -eq 0 ]; then

      gUniqueId=$( InsertItems "$gUniqueId" "FunctionExtended" "$FunctionsExtendedAreaX" "$boundingBoxFuncExtendedWidth" "$boundingBoxFuncExtendedHeight" "$totalGap" "${svgFilesFunctionsExtended[@]}" )

    else

      gUniqueId=$( InsertSymbolItems "$gUniqueId" "FunctionExtended" "$FunctionsExtendedAreaX" "$boundingBoxFuncExtendedWidth" "$boundingBoxFuncExtendedHeight" "$totalGap" "${arrSymbolFunctionsExtendedEntries[@]}" )

    fi

    echo "$templateCloseGroup" >> "$THEME_TEMPLATE"

  #fi

  # --------------------------------------------------

  # Insert Function icons

  # Patch Functions Y placement

  InsertSubSectionBreak "MAIN SCREEN - Functions"

  tmpSection=$( ApplyScale "$templateHeaderFunctions" )

  tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-Y-->/${gTHEME_FUNCTIONS_Y}/g" )

  echo "$tmpSection" >> "$THEME_TEMPLATE"

  LogMultiple "1" "---------------------------"
  LogMultiple "1" "Checking Width and Height of Functions"

  if [ $SYMBOL_FILE_FUNCTIONS -eq 0 ]; then

    boundingBoxFuncWidth=$( GetSvgViewBoxDimensionFromArray "Width" "$IMAGES_DIR"/"$FUNCTIONS_DIR_NAME" "${svgFilesFunctions[@]}" )
    boundingBoxFuncHeight=$( GetSvgViewBoxDimensionFromArray "Height" "$IMAGES_DIR"/"$FUNCTIONS_DIR_NAME" "${svgFilesFunctions[@]}" )

  else

    #symbolsfileExtensionRemoved="${SYMBOLS_FILENAME%*.svg}"
    #boundingBoxFuncWidth=$( GetSvgViewBoxWidth "$IMAGES_DIR"/"$SYMBOLS_DIR_NAME" "$symbolsfileExtensionRemoved" )
    #boundingBoxFuncHeight=$( GetSvgViewBoxHeight "$IMAGES_DIR"/"$SYMBOLS_DIR_NAME" "$symbolsfileExtensionRemoved" )

    boundingBoxFuncWidth=$gTHEME_FUNCTION_SIZE
    boundingBoxFuncHeight=$gTHEME_FUNCTION_SIZE

  fi

  LogMultiple "1" "Global Volume Bounding Box width: $boundingBoxFuncWidth"
  LogMultiple "1" "Global Volume Bounding Box Height: $boundingBoxFuncHeight"

  # Calculate X placement for Functions
  # Note. This is only for satisfaction of viewing theme file. Clover doesn't care about this placement.

  if [ $SYMBOL_FILE_FUNCTIONS -eq 0 ]; then

    nightImageCount=$( NightImagesInArray "${svgFilesFunctions[@]}" )
    dayImageCount=$(( ${#svgFilesFunctions[@]}-$nightImageCount ))

  else

    # Count number of Icons from List file
    nightImageCount=$( grep "_night" "$IMAGES_DIR"/"$SYMBOLS_DIR_NAME"/"$SYMBOL_FILE_FUNCTIONS_LIST" | wc -l )
    nightImageCount=$( echo "$nightImageCount" | tr -d '[:space:]' )

    dayImageCount=$( cat "$IMAGES_DIR"/"$SYMBOLS_DIR_NAME"/"$SYMBOL_FILE_FUNCTIONS_LIST" | wc -l )
    dayImageCount=$( echo "$dayImageCount" | tr -d '[:space:]' )
    dayImageCount=$(( dayImageCount - nightImageCount ))

  fi

  totalGap=$( CalculateGap "$gTHEME_SELECTIONSMALL_BOUNDINGBOX_WIDTH" "$boundingBoxFuncWidth" )

  numberOfFunctionsThatFit=$(( $gTHEME_WIDTH/($boundingBoxFuncWidth+$totalGap) ))

  if [ $SHOW_NIGHT_ICONS -eq 1 ]; then
    [[ $nightImageCount -lt $numberOfFunctionsThatFit ]] && numberOfFunctionsThatFit=$nightImageCount
  fi

  if [ $SHOW_NIGHT_ICONS -eq 0 ]; then
    [[ $dayImageCount -lt $numberOfFunctionsThatFit ]] && numberOfFunctionsThatFit=$dayImageCount
  fi

  functionAreaWidth=$(( ($boundingBoxFuncWidth+$totalGap)*$numberOfFunctionsThatFit ))
  functionAreaWidth=$(( $functionAreaWidth - $totalGap ))
  functionAreaX=$( XposForCentred "$functionAreaWidth" ) 

  # Find if functions are smaller than their respective canvas
  # For example, the designer has used a 128px canvas for many types of symbols
  # but the functions on this canvas are only 64px in size.
  # This difference means that when positioning the functions in the theme file,
  # the whole function icon group will be offset and not centred.
  # This is purely visual when looking at the svg file. Clover does not care.

  if [ $SYMBOL_FILE_FUNCTIONS -eq 1 ]; then

    # Loop through each symbol '<use xlink:href ....' line in the svg file

    declare -a functionMatrix
    largestSymWidth=0
    matrixAofLargest=0
    xAdj=0

    for (( s=0; s<${#arrSymbolsFunctionsUsage[@]}; s++ ))
    do

      # Get the transform matrix values

      tm="${arrSymbolsFunctionsUsage[$s]##*transform=\"matrix(}"
      tm="${tm%)*}"
      IFS=' ' read -r -a functionMatrix <<< "$tm"

      # Get the width

      symWidth="${arrSymbolsFunctionsUsage[$s]##*width=\"}"
      symWidth="${symWidth%%\"*}"

      if (( $(echo "$symWidth > $largestSymWidth" |bc -l) )); then
        largestSymWidth=$symWidth
        matrixAofLargest=${functionMatrix[0]}
      fi

      unset functionMatrix

    done

    LogMultiple "1" "Widest Symbol: $largestSymWidth has matrix [A] value: $matrixAofLargest"

    if [ $largestSymWidth != "0" ] && [ $matrixAofLargest != "0" ]; then

      xAdj=$( echo "scale=2; $largestSymWidth*$matrixAofLargest" | bc )
      xAdj=$( echo "scale=2; $largestSymWidth-$xAdj" | bc )
      xAdj=$( echo "scale=2; $xAdj/2" | bc )
      xAdj=$( printf "%.0f\n" $xAdj )

    fi

    if [ $xAdj -ne 0 ]; then

      # Check if this value is to be deducted or added

      if [ $(echo "$xAdj>1"| bc) -eq 0 ]; then

        LogMultiple "1" "Function horizontal adjustment: +$xAdj"
        functionAreaX=$(( functionAreaX+xAdj ))

      else

        LogMultiple "1" "Function horizontal adjustment: -$xAdj"
        functionAreaX=$(( functionAreaX-xAdj ))

      fi

    fi

  fi

  if [ $SYMBOL_FILE_FUNCTIONS -eq 0 ]; then

    gUniqueId=$( InsertItems "$gUniqueId" "Function" "$functionAreaX" "$boundingBoxFuncWidth" "$boundingBoxFuncHeight" "$totalGap" "${svgFilesFunctions[@]}" )

  else

    gUniqueId=$( InsertSymbolItems "$gUniqueId" "Function" "$functionAreaX" "$boundingBoxFuncWidth" "$boundingBoxFuncHeight" "$totalGap" "${arrSymbolFunctionsEntries[@]}" )

  fi

  echo "$templateCloseGroup" >> "$THEME_TEMPLATE"

  # --------------------------------------------------

  # Insert Selection_Small

  idx=0
  while [ "${svgFiles[$idx]}" != "selection_small" ] && [ $idx -le $gTotalRootSvgFiles ]
  do
    ((idx++))
  done

  if [ "${svgFiles[$idx]}" == "selection_small" ]; then

    InsertSubSectionBreak "MAIN SCREEN - Selection Small"

    PrintFileName "${svgFiles[$idx]}"

    # Selection Small Placement
    selectionSmallY=$(( $gTHEME_FUNCTIONS_Y-($totalGap/2) ))
    selectionSmallX=$(( (($gTHEME_WIDTH-$functionAreaWidth)/2)-($totalGap/2) ))

    tmpSvgData=$( GetAndPatchSvgFileContent "$IMAGES_DIR" "${svgFiles[$idx]}" $gUniqueId )

    ((gUniqueId++))

    tmpSection=$( ApplyScale "$templateSelectionSmall" )

    if [ $SHOW_NIGHT_ICONS -eq 1 ]; then

      tmpSection=$( echo "$tmpSection" | sed 's/class=\"\"/class=\"DisplayNone\"/' ) && WriteToThemeLog "Setting ${svgFiles[$idx]} as hidden"

    fi

    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILENAME-->/${svgFiles[$idx]}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-X-->/${selectionSmallX}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-Y-->/${selectionSmallY}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-WIDTH-->/${gTHEME_SELECTIONSMALL_BOUNDINGBOX_WIDTH}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-HEIGHT-->/${gTHEME_SELECTIONSMALL_BOUNDINGBOX_HEIGHT}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILECONTENT-->/${tmpSvgData}/g" )

    # Insert in to Theme template

    echo "$tmpSection" >> "$THEME_TEMPLATE"

  fi

  # --------------------------------------------------

  # Insert Selection_Small_night

  idx=0
  while [ "${svgFiles[$idx]}" != "selection_small_night" ] && [ $idx -le $gTotalRootSvgFiles ]
  do
    ((idx++))
  done

  if [ "${svgFiles[$idx]}" == "selection_small_night" ]; then

    PrintFileName "${svgFiles[$idx]}"

    # Selection Small Placement
    selectionSmallY=$(( $gTHEME_FUNCTIONS_Y-($totalGap/2) ))
    selectionSmallX=$(( (($gTHEME_WIDTH-$functionAreaWidth)/2)-($totalGap/2) ))

    tmpSvgData=$( GetAndPatchSvgFileContent "$IMAGES_DIR" "${svgFiles[$idx]}" $gUniqueId )

    ((gUniqueId++))

    tmpSection=$( ApplyScale "$templateSelectionSmallNight" )

  #   if [ $SHOW_NIGHT_ICONS -eq 0 ]; then
  # 
  #     tmpSection=$( echo "$tmpSection" | sed "s/transform/class=\"DisplayNone\" transform/" ) && WriteToThemeLog "Setting ${svgFiles[$idx]} as hidden"
  # 
  #   fi

    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILENAME-->/${svgFiles[$idx]}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-X-->/${selectionSmallX}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-Y-->/${selectionSmallY}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-WIDTH-->/${gTHEME_SELECTIONSMALL_BOUNDINGBOX_WIDTH}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-HEIGHT-->/${gTHEME_SELECTIONSMALL_BOUNDINGBOX_HEIGHT}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILECONTENT-->/${tmpSvgData}/g" )

    # Insert in to Theme template

    echo "$tmpSection" >> "$THEME_TEMPLATE"

  fi

  # --------------------------------------------------

  # Insert Message Row

  InsertSubSectionBreak "MAIN SCREEN - Message Row"

  tmpSection=$( ApplyScale "$templateMessageRow" )

  tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-MESSAGE-->/${MESSAGE_ROW_TEXT}/g" )

  # Find width of message

  tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-X-->/${MESSAGE_ROW_XPOS}/g" )

  # Hide Message Row if only showing night icons

  if [ $SHOW_NIGHT_ICONS -eq 1 ]; then

    tmpSection=$( echo "$tmpSection" | sed "s/DisplayBlock/DisplayNone/" )

  fi

  echo "$tmpSection" >> "$THEME_TEMPLATE"

  # --------------------------------------------------

  # Insert Message Row Night

  tmpSection=$( ApplyScale "$templateMessageRowNight" )

  tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-MESSAGE-->/${MESSAGE_ROW_TEXT}/g" )

  tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-X-->/${MESSAGE_ROW_XPOS}/g" )

  # Hide Message Row if only showing day icons

  if [ $SHOW_NIGHT_ICONS -eq 0 ]; then

    tmpSection=$( echo "$tmpSection" | sed "s/DisplayBlock/DisplayNone/" )

  fi

  echo "$tmpSection" >> "$THEME_TEMPLATE"

  # --------------------------------------------------

  # Insert OS Badges

  # Patch OSBadge Y placement

  InsertSubSectionBreak "MAIN SCREEN - OSBadges"

  tmpSection=$( ApplyScale "$templateHeaderOSBadges" )

  tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-Y-->/${gTHEME_OSBADGE_Y}/g" )

  # Set visibility depending on Badges setting.

  if [ "$gTHEME_BADGE_SWAP_STATE" == "none" ]; then

    tmpSection=$( echo "$tmpSection" | sed "s/transform/class=\"DisplayNone\" transform/g" )

  fi

  echo "$tmpSection" >> "$THEME_TEMPLATE"

  LogMultiple "1" "---------------------------"
  LogMultiple "1" "Checking Width and Height of OsBadges"

  if [ $SYMBOL_FILE_BADGES -eq 0 ]; then

    boundingBoxBadgeWidth=$( GetSvgViewBoxDimensionFromArray "Width" "$IMAGES_DIR"/"$OSBADGES_DIR_NAME" "${svgFilesOsBadges[@]}" )
    boundingBoxBadgeHeight=$( GetSvgViewBoxDimensionFromArray "Height" "$IMAGES_DIR"/"$OSBADGES_DIR_NAME" "${svgFilesOsBadges[@]}" )

  else

    symbolsfileExtensionRemoved="${SYMBOLS_FILENAME%*.svg}"
    boundingBoxBadgeWidth=$( GetSvgViewBoxWidth "$IMAGES_DIR"/"$SYMBOLS_DIR_NAME" "$symbolsfileExtensionRemoved" )
    boundingBoxBadgeHeight=$( GetSvgViewBoxHeight "$IMAGES_DIR"/"$SYMBOLS_DIR_NAME" "$symbolsfileExtensionRemoved" )

  fi

  LogMultiple "1" "Global OsBadge Bounding Box width: $boundingBoxBadgeWidth"
  LogMultiple "1" "Global OsBadge Bounding Box Height: $boundingBoxBadgeHeight"

  # Calculate X placement for OsBadges
  # Note. This is only for satisfaction of viewing theme file. Clover doesn't care about this placement.

  if [ $SYMBOL_FILE_BADGES -eq 0 ]; then

    nightImageCount=$( NightImagesInArray "${svgFilesOsBadges[@]}" )
    dayImageCount=$(( ${#svgFilesOsBadges[@]}-$nightImageCount ))

  else

    # Count number of Icons from List file
    nightImageCount=$( grep "_night" "$IMAGES_DIR"/"$SYMBOLS_DIR_NAME"/"$SYMBOL_FILE_BADGES_LIST" | wc -l )
    nightImageCount=$( echo "$nightImageCount" | tr -d '[:space:]' )

    dayImageCount=${#arrSymbolOsBadgeListItems[@]}
    dayImageCount=$(( dayImageCount - nightImageCount ))

  fi

  totalGap=$( CalculateGap "$gTHEME_SELECTIONBIG_BOUNDINGBOX_WIDTH" "$boundingBoxBadgeWidth" )

  numberOfBadgesThatFit=$(( $gTHEME_WIDTH/($boundingBoxBadgeWidth+$totalGap) ))

  if [ $SHOW_NIGHT_ICONS -eq 1 ]; then
    [[ $nightImageCount -lt $numberOfBadgesThatFit ]] && numberOfBadgesThatFit=$nightImageCount
  fi

  if [ $SHOW_NIGHT_ICONS -eq 0 ]; then
    [[ $dayImageCount -lt $numberOfBadgesThatFit ]] && numberOfBadgesThatFit=$dayImageCount
  fi

  osBadgeAreaWidth=$(( ($boundingBoxBadgeWidth+$totalGap)*$numberOfBadgesThatFit ))
  osBadgeAreaWidth=$(( $osBadgeAreaWidth - $totalGap ))
  osBadgeAreaX=$( XposForCentred "$osBadgeAreaWidth" ) 

  if [ $SYMBOL_FILE_BADGES -eq 0 ]; then

    gUniqueId=$( InsertItems "$gUniqueId" "Badge" "$osBadgeAreaX" "$boundingBoxBadgeWidth" "$boundingBoxBadgeHeight" "$totalGap" "${svgFilesOsBadges[@]}" )

  else

    gUniqueId=$( InsertSymbolItems "$gUniqueId" "Badge" "$osBadgeAreaX" "$boundingBoxBadgeWidth" "$boundingBoxBadgeHeight" "$totalGap" "${arrSymbolOsBadgeEntries[@]}" )

  fi

  echo "$templateCloseGroup" >> "$THEME_TEMPLATE"

  # --------------------------------------------------

  # Insert Selection_Big

  idx=0
  while [ "${svgFiles[$idx]}" != "selection_big" ] && [ $idx -le $gTotalRootSvgFiles ]
  do
    ((idx++))
  done

  if [ "${svgFiles[$idx]}" == "selection_big" ]; then

    InsertSubSectionBreak "MAIN SCREEN - Selection Big"

    PrintFileName "${svgFiles[$idx]}"

    # Selection Big Placement

    if [ $gTHEME_SELECTIONBIG_BOUNDINGBOX_HEIGHT -ge $boundingBoxBadgeHeight ]; then

      diffHeight=$(( $gTHEME_SELECTIONBIG_BOUNDINGBOX_HEIGHT-$boundingBoxBadgeHeight ))

      if [ "$gTHEME_BADGE_SWAP_STATE" == "swap" ]; then
        selectionBigY=$(( $gTHEME_OSBADGE_Y-($diffHeight/2) ))
      else
        selectionBigY=$(( $gTHEME_VOLUMES_Y-($diffHeight/2) ))
      fi

    else

      diffHeight=$(( $boundingBoxBadgeHeight-$gTHEME_SELECTIONBIG_BOUNDINGBOX_HEIGHT ))

      if [ "$gTHEME_BADGE_SWAP_STATE" == "swap" ]; then
        selectionBigY=$(( $gTHEME_OSBADGE_Y+($diffHeight/2) ))
      else
        selectionBigY=$(( $gTHEME_VOLUMES_Y+($diffHeight/2) ))
      fi

    fi

    if [ $gTHEME_SELECTIONBIG_BOUNDINGBOX_WIDTH -ge $boundingBoxBadgeWidth ]; then

      diffWidth=$(( $gTHEME_SELECTIONBIG_BOUNDINGBOX_WIDTH-$boundingBoxBadgeWidth ))
      selectionBigX=$(( (($gTHEME_WIDTH-$osBadgeAreaWidth)/2)-($diffWidth/2) ))

    else

      diffWidth=$(( $boundingBoxBadgeWidth-$gTHEME_SELECTIONBIG_BOUNDINGBOX_WIDTH ))
      selectionBigX=$(( (($gTHEME_WIDTH-$osBadgeAreaWidth)/2)+($diffWidth/2) ))

    fi

    tmpSvgData=$( GetAndPatchSvgFileContent "$IMAGES_DIR" "${svgFiles[$idx]}" $gUniqueId )

    ((gUniqueId++))

    tmpSection=$( ApplyScale "$templateSelectionBig" )

    if [ $SHOW_NIGHT_ICONS -eq 1 ]; then

      tmpSection=$( echo "$tmpSection" | sed "s/transform/class=\"DisplayNone\" transform/" ) && LogMultiple "1" "Setting ${svgFiles[$idx]} as hidden"

    fi

    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILENAME-->/${svgFiles[$idx]}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-X-->/${selectionBigX}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-Y-->/${selectionBigY}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-WIDTH-->/${gTHEME_SELECTIONBIG_BOUNDINGBOX_WIDTH}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-HEIGHT-->/${gTHEME_SELECTIONBIG_BOUNDINGBOX_HEIGHT}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILECONTENT-->/${tmpSvgData}/g" )

    # Insert in to Theme template

    echo "$tmpSection" >> "$THEME_TEMPLATE"

  fi

  # --------------------------------------------------

  # Insert Selection_Big_Night

  idx=0
  while [ "${svgFiles[$idx]}" != "selection_big_night" ] && [ $idx -le $gTotalRootSvgFiles ]
  do
    ((idx++))
  done

  if [ "${svgFiles[$idx]}" == "selection_big_night" ]; then

    PrintFileName "${svgFiles[$idx]}"

    tmpSvgData=$( GetAndPatchSvgFileContent "$IMAGES_DIR" "${svgFiles[$idx]}" $gUniqueId )

    ((gUniqueId++))

    tmpSection=$( ApplyScale "$templateSelectionBigNight" )

    if [ $SHOW_NIGHT_ICONS -eq 0 ]; then

      tmpSection=$( echo "$tmpSection" | sed "s/transform/class=\"DisplayNone\" transform/" ) && LogMultiple "1" "Setting ${svgFiles[$idx]} as hidden"

    fi

    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILENAME-->/${svgFiles[$idx]}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-X-->/${selectionBigX}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-Y-->/${selectionBigY}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-WIDTH-->/${gTHEME_SELECTIONBIG_BOUNDINGBOX_WIDTH}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-HEIGHT-->/${gTHEME_SELECTIONBIG_BOUNDINGBOX_HEIGHT}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILECONTENT-->/${tmpSvgData}/g" )

    # Insert in to Theme template

    echo "$tmpSection" >> "$THEME_TEMPLATE"

  fi

  # --------------------------------------------------

  # Insert Selection_Indicator

  idx=0
  while [ "${svgFiles[$idx]}" != "selection_indicator" ] && [ $idx -le $gTotalRootSvgFiles ]
  do
    ((idx++))
  done

  if [ "${svgFiles[$idx]}" == "selection_indicator" ]; then

    InsertSubSectionBreak "MAIN SCREEN - Selection Indicator"

    PrintFileName "${svgFiles[$idx]}"

    tmpSvgData=$( GetAndPatchSvgFileContent "$IMAGES_DIR" "${svgFiles[$idx]}" $gUniqueId )

    ((gUniqueId++))

    tmpSection=$( ApplyScale "$templateSelectionIndicator" )

    if [ $SHOW_NIGHT_ICONS -eq 0 ]; then

      tmpSection=$( echo "$tmpSection" | sed "s/transform/class=\"DisplayNone\" transform/" ) && LogMultiple "1" "Setting ${svgFiles[$idx]} as hidden"

    fi

    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILENAME-->/${svgFiles[$idx]}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILECONTENT-->/${tmpSvgData}/g" )

    # Insert in to Theme template

    echo "$tmpSection" >> "$THEME_TEMPLATE"

  fi

  # --------------------------------------------------

  # Insert Selection_Indicator_night

  idx=0
  while [ "${svgFiles[$idx]}" != "selection_indicator_night" ] && [ $idx -le $gTotalRootSvgFiles ]
  do
    ((idx++))
  done

  if [ "${svgFiles[$idx]}" == "selection_indicator_night" ]; then

    PrintFileName "${svgFiles[$idx]}"

    tmpSvgData=$( GetAndPatchSvgFileContent "$IMAGES_DIR" "${svgFiles[$idx]}" $gUniqueId )

    ((gUniqueId++))

    tmpSection=$( ApplyScale "$templateSelectionIndicatorNight" )

    if [ $SHOW_NIGHT_ICONS -eq 0 ]; then

      tmpSection=$( echo "$tmpSection" | sed "s/transform/class=\"DisplayNone\" transform/" ) && LogMultiple "1" "Setting ${svgFiles[$idx]} as hidden"

    fi

    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILENAME-->/${svgFiles[$idx]}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILECONTENT-->/${tmpSvgData}/g" )

    # Insert in to Theme template

    echo "$tmpSection" >> "$THEME_TEMPLATE"

  fi

  echo "$templateCloseGroup" >> "$THEME_TEMPLATE"

  # --------------------------------------------------

  # Insert Common Items

  # Note: This group holds the banner and pointer
  #       Therefore, the position has to accommodate the width of the banner

  #echo "$tmpSection" >> "$THEME_TEMPLATE"

  # --------------------------------------------------

  # Insert Banner (and Common Items group once calculated size of banner)

  idx=0
  while [ "${svgFiles[$idx]}" != "Banner" ] && [ $idx -le $gTotalRootSvgFiles ]
  do
    ((idx++))
  done

  if [ "${svgFiles[$idx]}" == "Banner" ]; then

    PrintFileName "${svgFiles[$idx]}"

    tmpSection=$( echo "$templateBanner" | sed "s/<!--INSERT-FILENAME-->/${svgFiles[$idx]}/g" )

    if [ $SHOW_NIGHT_ICONS -eq 1 ]; then

      tmpSection=$( echo "$tmpSection" | sed 's/class=\"\"/class=\"DisplayNone\"/' ) && WriteToThemeLog "Setting ${svgFiles[$idx]} as hidden"

    fi

    bannerSvgViewBoxWidth=$( GetSvgViewBoxWidth "$IMAGES_DIR" "${svgFiles[$idx]}" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-WIDTH-->/${bannerSvgViewBoxWidth}/g" )

    bannerSvgViewBoxHeight=$( GetSvgViewBoxHeight "$IMAGES_DIR" "${svgFiles[$idx]}" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-HEIGHT-->/${bannerSvgViewBoxHeight}/g" )

    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-X-->/0/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-Y-->/0/g" )

    tmpSvgData=$( GetAndPatchSvgFileContent "$IMAGES_DIR" "${svgFiles[$idx]}" $gUniqueId )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILECONTENT-->/${tmpSvgData}/g" )

    ((gUniqueId++))

    # Centre Common Items group horizontally

    #commonItemsPosX=$((($gTHEME_WIDTH-($bannerSvgViewBoxWidth))/2))
    commonItemsPosX=$( XposForCentred "$bannerSvgViewBoxWidth" ) 

    [[ $commonItemsPosX -lt 0 ]] && commonItemsPosX=0

    InsertSectionBreak "COMMON ITEMS"

    tmpSectionHeader=$( ApplyScale "$templateHeaderCommonItems" )

    commonItemsSection=$( echo "$tmpSectionHeader" | sed "s/<!--INSERT-X-->/${commonItemsPosX}/g" )

    echo "$commonItemsSection" >> "$THEME_TEMPLATE"

    # Insert in to Theme template

    echo "$tmpSection" >> "$THEME_TEMPLATE"

  fi

  # --------------------------------------------------

  # Insert Banner Night

  idx=0
  while [ "${svgFiles[$idx]}" != "Banner_night" ] && [ $idx -le $gTotalRootSvgFiles ]
  do
    ((idx++))
  done

  if [ "${svgFiles[$idx]}" == "Banner_night" ]; then

    PrintFileName "${svgFiles[$idx]}"

    tmpSection=$( echo "$templateBannerNight" | sed "s/<!--INSERT-FILENAME-->/${svgFiles[$idx]}/g" )

    if [ $SHOW_NIGHT_ICONS -eq 0 ]; then

      tmpSection=$( echo "$tmpSection" | sed "s/transform/class=\"DisplayNone\" transform/" ) && WriteToThemeLog "Setting ${svgFiles[$idx]} as hidden"

    fi

    bannerSvgViewBoxWidth=$( GetSvgViewBoxWidth "$IMAGES_DIR" "${svgFiles[$idx]}" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-WIDTH-->/${bannerSvgViewBoxWidth}/g" )

    bannerSvgViewBoxHeight=$( GetSvgViewBoxHeight "$IMAGES_DIR" "${svgFiles[$idx]}" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-HEIGHT-->/${bannerSvgViewBoxHeight}/g" )

    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-X-->/0/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-Y-->/0/g" )

    tmpSvgData=$( GetAndPatchSvgFileContent "$IMAGES_DIR" "${svgFiles[$idx]}" $gUniqueId )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILECONTENT-->/${tmpSvgData}/g" )

    ((gUniqueId++))

    # Insert in to Theme template

    echo "$tmpSection" >> "$THEME_TEMPLATE"

  fi

  # --------------------------------------------------

  # Insert Pointer

  idx=0
  while [ "${svgFiles[$idx]}" != "pointer" ] && [ $idx -le $gTotalRootSvgFiles ]
  do
    ((idx++))
  done

  if [ "${svgFiles[$idx]}" == "pointer" ]; then

    PrintFileName "${svgFiles[$idx]}"

    tmpSvgData=$( GetAndPatchSvgFileContent "$IMAGES_DIR" "${svgFiles[$idx]}" $gUniqueId )

    ((gUniqueId++))

    tmpSection=$( echo "$templatePointer" | sed "s/<!--INSERT-FILENAME-->/${svgFiles[$idx]}/g" )
    tmpSection=$( echo "$tmpSection" | sed "s/<!--INSERT-FILECONTENT-->/${tmpSvgData}/g" )

    # Insert in to Theme template

    echo "$tmpSection" >> "$THEME_TEMPLATE"

  fi


  echo "$templateCloseGroup" >> "$THEME_TEMPLATE"

  echo "$templateCloseSvg" >> "$THEME_TEMPLATE"

  # Check for global replace of colours

  if [ -f "$COLOUR_CHANGE_FILE" ]; then

    ReplaceThemeColours

  fi

  # For easier readability of file
  # Global replace @@~~ symbols (which were inserted between symbol layers) with newlines

  sed -i -e $'s/@@~~/\\\n/g' "$TMP_DIR"/"$FINAL_THEME_FILENAME"

  # Copy final Theme template to Result Dir and Clean up

  cp "$TMP_DIR"/"$FINAL_THEME_FILENAME" "$RESULT_DIR" && rm -rf "$TMP_DIR" && mkdir "$TMP_DIR"
}

# ====================================
# MAIN
# ====================================

# Set Terminal Window Size to 100x50 characters

printf '\e[8;50;100t'

[[ -f "$logScriptFile" ]] && rm "$logScriptFile"

LogMultiple "0" "$doubleLines"
LogMultiple "0" "Clover SVG Theme Builder v$VER"
LogMultiple "0" "Blackosx 2018"
LogMultiple "0" "$doubleLines"

LogMultiple "0" "$(date '+%Y-%m-%d %H:%M:%S')"

# Ensure Required Resources

if [ ! -d "$RESOURCES_DIR" ]; then

    LogMultiple "0" "Resources Directory is missing!. Exiting"
    exit 1

elif [ ! -f "$TEMPLATE_PARTS" ]; then

    LogMultiple "0" "Parts Template file is missing! Exiting."
    exit 1

fi

if [ ! -d "$THEMES_DIR" ]; then

    LogMultiple "0" "Creating Themes directory"
    mkdir "$THEMES_DIR"

fi

[[ -d "$TMP_DIR" ]] && LogMultiple "0" "Removing previous temporary dir" && rm -rf "$TMP_DIR"
mkdir "$TMP_DIR"

[[ -d "$TMP_DIR_UPDATE" ]] && LogMultiple "0" "Removing previous update dir" && rm -rf "$TMP_DIR_UPDATE"
mkdir "$TMP_DIR_UPDATE"

[[ -d "$RESULTS_DIR" ]] && LogMultiple "0" "Removing previous results dir" && rm -rf "$RESULTS_DIR"
mkdir "$RESULTS_DIR"

# --------------------------------------------------

# Check for Update

CheckForUpdate &

# --------------------------------------------------

# Find Themes to process

if [ -d "$THEMES_DIR" ]; then

  dirCount=$( find "$THEMES_DIR" -depth 1 -type d 2>/dev/null | wc -l )
  dirCount=$( echo "$dirCount" | tr -d '[:space:]' )

  if [ $dirCount -eq 0 ] ; then

    LogMultiple "0" "There are no themes in the ${THEMES_DIR##*/} directory!"

    # Give user option of downloading Sample Theme

    UserInput=""

    PrintToStdOut "Do you wish to download the sample theme? (Y or N)?"

    read UserInput

    while [[ ! "$UserInput" =~ ^[YyNn] ]]
    do

      PrintToStdOut "Do you wish to download the sample theme? (Y or N)"
      read UserInput

    done

    if [[ "$UserInput" =~ ^[Yy] ]]; then

      [[ ! -d "$TMP_DIR" ]] && mkdir "$TMP_DIR"

      # Download file from repo

      LogMultiple "0" "Attempting to download sample theme"

      curl -L -o "$TMP_DIR"/Theme.zip https://bitbucket.org/blackosx/cloversvgthemebuilder/downloads/Theme.zip

      if [ -f "$TMP_DIR"/Theme.zip ]; then

        LogMultiple "0" "Uncompressing sample theme"

        unzip -q -d "$TMP_DIR" "$TMP_DIR"/Theme.zip && rm -rf "$TMP_DIR"/Theme.zip && mv "$TMP_DIR"/Theme "$THEMES_DIR"

      fi

    else

      LogMultiple "0" "Cannot continue without Theme dir" 
      LogMultiple "0" "Exiting."
      exit 1

    fi

  fi

	for themeDir in "$THEMES_DIR"/* ; do

    ProcessTheme "$themeDir"

	done

  open "$RESULTS_DIR"

  LogMultiple "0" ""
  LogMultiple "0" "$doubleLines"
  LogMultiple "0" "Process themes complete"
  LogMultiple "0" "$doubleLines"
  LogMultiple "0" ""

  sleep 1

  # Check result of script update

  if [ -f "$TMP_DIR_UPDATE"/gitPull ]; then

    LogMultiple "0" "There's an update available to this script."

    # Give user option of updating script

    UserInput=""

    PrintToStdOut "Would you like to git pull? (Y or N)?"

    read UserInput

    while [[ ! "$UserInput" =~ ^[YyNn] ]]
    do

      PrintToStdOut "Would you like to git pull? (Y or N)?"
      read UserInput

    done

    if [[ "$UserInput" =~ ^[Yy] ]]; then

      git pull

    else

      PrintToStdOut "Not updating." 

    fi

  elif [ -f "$TMP_DIR_UPDATE"/source ]; then

    SCRIPT_VER_REPO=$( grep -m1 "VER=" "$TMP_DIR_UPDATE"/source )
    SCRIPT_VER_REPO=$( echo "${SCRIPT_VER_REPO##*=}" | tr -d '".' )

    if ! [[ "$SCRIPT_VER_REPO" =~ [^[:digit:]] ]] && [ ! -z "$SCRIPT_VER_REPO" ] ; then

      if [ "$SCRIPT_VER_REPO" -gt "$SCRIPT_VER_CURRENT" ]; then

        LogMultiple "0" "Current version: $SCRIPT_VER_CURRENT | Latest Repo version: $SCRIPT_VER_REPO"
        LogMultiple "0" "There is a new version of this script available on the repo"

        # Give user option of updating script

        UserInput=""

        PrintToStdOut "Do you wish to update the script? (Y or N)?"

        read UserInput

        while [[ ! "$UserInput" =~ ^[YyNn] ]]
        do

          PrintToStdOut "Do you wish to update the script? (Y or N)?"
          read UserInput

        done

        if [[ "$UserInput" =~ ^[Yy] ]]; then

          cat "$TMP_DIR_UPDATE"/source > "$SCRIPT_DIR"/"$SCRIPT_NAME" && echo "Update. Done"

        else

          PrintToStdOut "Not updating." 

        fi

      fi

    fi

  fi

	rm -rf "$TMP_DIR"

fi


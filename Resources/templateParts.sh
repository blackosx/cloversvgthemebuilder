#!/bin/bash

# Clover SVG Theme Builder Template

# v1.0.0 - Initial element structure taken from Clovy theme
# v1.0.1 - CSS cleaned and simplified
# v1.0.2 - Replace some constants with markers for dynamic changes
# v1.0.3 - Remove style=\"enable-background:new 0 0 1200 768;\" from Header
# v1.0.4 - Add scale transform
# v1.0.5 - Apply font size style for MenuTitle and strip unpopulated menu rows.
# v1.0.6 - Move font css to external file.
# v1.0.7 - Add DisplayInline class to templateVolume
# v1.0.8 - Add DisplayInline class to templateFunction
# v1.0.9 - Add MessageRow_night
# v1.1.0 - Add class=\"DisplayBlock\" to MessageRows
# v1.1.1 - Add Menu Control item template entries
# v1.1.2 - Add Night Menu template entries and remove class from Checkbox/RadioButtons
# v1.1.3 - Add Selection Indicator template
# v1.1.4 - Change MessageRow templates to be dynamic
# v1.1.5 - Update URL to Clover Github Repository
# v1.1.6 - Add Selection Indicator Night template
# v1.1.7 - Change MessageRow Y position
# v1.1.8 - Change selection small class to NoFill

TEMPLATE_VER="1.1.8"

# Document Header

templateDocumentHeader="<?xml version=\"1.0\" encoding=\"utf-8\"?>"

# SVG Header

templateSvgHeader="<svg version=\"1.1\"
  id=\"CloverTheme(CloverSvgThemeBuilder $VER / [Template $TEMPLATE_VER])\"
  xmlns=\"http://www.w3.org/2000/svg\"
  xmlns:xlink=\"http://www.w3.org/1999/xlink\"
  xmlns:clover=\"https://github.com/CloverHackyColor/CloverBootloader\"
  x=\"0px\"
  y=\"0px\"
  viewBox=\"0 0 <!--INSERT-WIDTH--> <!--INSERT-HEIGHT-->\"
  xml:space=\"preserve\">"

# CSS

templateCSS="<style type=\"text/css\">
  .NoFill{fill:none;}
  .DisplayInline{display:inline;}
  .DisplayNone{display:none;}
  .MenuContainerBorder{fill:none;stroke:#FFFFFF;stroke-opacity:0.328;}
  .MenuSelectionBoundingBox{fill:#FBFBFB;fill-opacity:0.3298;}
  .MenuSelectionBoundingBoxNight{fill:#0B1D37;fill-opacity:FF;}"

templateCSSClose="</style>"

# Font

templateFont="<font>
<!-- INSERT FONT DATA -->
</font>"

# Background

templateBackground="<g id=\"Background\" transform=\"scale(1)\">
  <rect visibility=\"hidden\" id=\"BoundingRect\" x=\"0\" y=\"0\" class=\"NoFill\" width=\"1200\" height=\"768\"/>
<!--INSERT-FILECONTENT-->
</g>"

# Background Night

templateBackgroundNight="<g id=\"Background_night\" transform=\"scale(1)\">
  <rect visibility=\"hidden\" id=\"BoundingRect\" x=\"0\" y=\"0\" class=\"NoFill\" width=\"1200\" height=\"768\"/>
<!--INSERT-FILECONTENT-->
</g>"

# Headers

templateHeaderMenuScreen="<g id=\"MenuScreen\" transform=\"scale(1) translate(0.000000, 280.000000)\" class=\"DisplayNone\">"

templateHeaderMenuContainer="  <g id=\"MenuContainer\" transform=\"translate(330.000000, 88.000000)\" class=\"DisplayInline\">"

templateHeaderControls="  <g id=\"Controls\" transform=\"translate(330.000000, 88.000000)\" class=\"DisplayInline\">"

templateHeaderMenuScroll="    <g id=\"MenuScroll\" transform=\"translate(540.000000, 0.000000)\">"

templateHeaderMenuScrollNight="    <g id=\"MenuScroll_night\" transform=\"translate(540.000000, 0.000000)\">"

templateHeaderFunctionsExtended="  <g id=\"FunctionsExtended\" transform=\"scale(1) translate(0, <!--INSERT-Y-->)\" class=\"DisplayNone\">"

templateHeaderMainScreen="<g id=\"MainScreen\">"

templateHeaderVolumes="  <g id=\"Volumes\" transform=\"scale(1) translate(0, <!--INSERT-Y-->)\">"

templateHeaderFunctions="  <g id=\"Functions\" transform=\"scale(1) translate(0, <!--INSERT-Y-->)\">"

templateHeaderOSBadges="<g id=\"OSBadges\" transform=\"scale(1) translate(0.000000, <!--INSERT-Y-->)\">"

templateHeaderCommonItems="<g id=\"CommonItems\" transform=\"scale(1) translate(<!--INSERT-X-->, 117.000000)\">"

# Menu

templateContainerBorder="    <rect id=\"ContainerBorder\" x=\"0.5\" y=\"0.5\" class=\"MenuContainerBorder\" width=\"551\" height=\"358\"/>"

templateMenuSelection="    <g id=\"MenuSelection\" transform=\"translate(1.000000, 0.000000)\">
      <rect id=\"BoundingBox\" y=\"0\" class=\"MenuSelectionBoundingBox\" width=\"551\" height=\"25\"/>
    </g>"

templateMenuRows="    <g id=\"MenuRows\" transform=\"translate(29.000000, 0.000000)\">
      <text transform=\"matrix(1 0 0 1 0 17)\" class=\"FontColour FontName FontSizeMenu\">Boot Aguments: </text>
      <text transform=\"matrix(1 0 0 1 0 41)\" class=\"FontColour FontName FontSizeMenu\">Confgs ...</text>
      <text transform=\"matrix(1 0 0 1 0 65)\" class=\"FontColour FontName FontSizeMenu\">GUI tuning ...</text>
      <text transform=\"matrix(1 0 0 1 0 89)\" class=\"FontColour FontName FontSizeMenu\">ACPI patching ...</text>
      <text transform=\"matrix(1 0 0 1 0 113)\" class=\"FontColour FontName FontSizeMenu\">SMBIOS ...</text>
      <text transform=\"matrix(1 0 0 1 0 137)\" class=\"FontColour FontName FontSizeMenu\">PCI devices ...</text>
      <text transform=\"matrix(1 0 0 1 0 161)\" class=\"FontColour FontName FontSizeMenu\">CPU tuning ...</text>
      <text transform=\"matrix(1 0 0 1 0 185)\" class=\"FontColour FontName FontSizeMenu\">Graphics Injector ...</text>
      <text transform=\"matrix(1 0 0 1 0 209)\" class=\"FontColour FontName FontSizeMenu\">Audio tuning ...</text>
      <text transform=\"matrix(1 0 0 1 0 233)\" class=\"FontColour FontName FontSizeMenu\">Binaries patching ...</text>
      <text transform=\"matrix(1 0 0 1 0 257)\" class=\"FontColour FontName FontSizeMenu\">System parameters ...</text>
      <text transform=\"matrix(1 0 0 1 0 281)\" class=\"FontColour FontName FontSizeMenu\">Return</text>
    </g>"

templateMenuSelectionNight="    <g id=\"MenuSelectionNight\" transform=\"translate(1.000000, 0.000000)\">
      <rect id=\"BoundingBox\" y=\"0\" class=\"MenuSelectionBoundingBox\" width=\"551\" height=\"25\"/>
    </g>"

templateMenuRowsNight="    <g id=\"MenuRows_night\" transform=\"translate(29.000000, 0.000000)\">
      <text transform=\"matrix(1 0 0 1 0 17)\" class=\"FontColourNight FontName FontSizeMenu\">Boot Aguments: </text>
      <text transform=\"matrix(1 0 0 1 0 41)\" class=\"FontColourNight FontName FontSizeMenu\">Confgs ...</text>
      <text transform=\"matrix(1 0 0 1 0 65)\" class=\"FontColourNight FontName FontSizeMenu\">GUI tuning ...</text>
      <text transform=\"matrix(1 0 0 1 0 89)\" class=\"FontColourNight FontName FontSizeMenu\">ACPI patching ...</text>
      <text transform=\"matrix(1 0 0 1 0 113)\" class=\"FontColourNight FontName FontSizeMenu\">SMBIOS ...</text>
      <text transform=\"matrix(1 0 0 1 0 137)\" class=\"FontColourNight FontName FontSizeMenu\">PCI devices ...</text>
      <text transform=\"matrix(1 0 0 1 0 161)\" class=\"FontColourNight FontName FontSizeMenu\">CPU tuning ...</text>
      <text transform=\"matrix(1 0 0 1 0 185)\" class=\"FontColourNight FontName FontSizeMenu\">Graphics Injector ...</text>
      <text transform=\"matrix(1 0 0 1 0 209)\" class=\"FontColourNight FontName FontSizeMenu\">Audio tuning ...</text>
      <text transform=\"matrix(1 0 0 1 0 233)\" class=\"FontColourNight FontName FontSizeMenu\">Binaries patching ...</text>
      <text transform=\"matrix(1 0 0 1 0 257)\" class=\"FontColourNight FontName FontSizeMenu\">System parameters ...</text>
      <text transform=\"matrix(1 0 0 1 0 281)\" class=\"FontColourNight FontName FontSizeMenu\">Return</text>
    </g>"

templateMenuScroll="    <g id=\"MenuScroll\" transform=\"translate(540.000000, 0.000000)\">
      <rect id=\"Bar\" y=\"0\" class=\"MenuScrollBar\" width=\"12\" height=\"359\"/>
      <path id=\"Handler\" class=\"MenuScrollBarHandler\" d=\"M5.5,2L5.5,2C7.985,2,10,4.015,10,6.5v59c0,2.485-2.015,4.5-4.5,4.5l0,0C3.015,70,1,67.985,1,65.5v-59C1,4.015,3.015,2,5.5,2z\"/>
    </g>"

templateMenuControlItem="    <g id=\"<!--INSERT-FILENAME-->\" transform=\"translate(0.000000, 97.000000)\">
      <rect visibility=\"hidden\" id=\"BoundingRect_1_\" y=\"0\" class=\"NoFill\" width=\"<!--INSERT-WIDTH-->\" height=\"<!--INSERT-HEIGHT-->\"/>
<!--INSERT-FILECONTENT-->
    </g>"

templateMenuIcon="  <g id=\"MenuIcon\" transform=\"translate(576.000000, 0.000000)\" class=\"DisplayInline\">
    <rect visibility=\"hidden\" id=\"BoundingRect_5_\" y=\"0\" class=\"NoFill\" width=\"48\" height=\"48\"/>
<!-- INSERT ICON -->
  </g>"

templateMenuTitle="  <g id=\"MenuTitle\" transform=\"translate(0.000000, 50.000000)\" class=\"DisplayInline\">
    <g>
      <text id=\"TitleText_7_\" transform=\"matrix(1 0 0 1 569.2043 21)\" class=\"FontColour FontName FontSize\">Options</text>
    </g>
  </g>"

# Menu Controls

templateCheckbox="    <g id=\"<!--INSERT-FILENAME-->\" transform=\"translate(0, 97.000000)\">
      <rect visibility=\"hidden\" id=\"BoundingRect_1_\" y=\"0\" class=\"NoFill\" width=\"<!--INSERT-WIDTH-->\" height=\"<!--INSERT-HEIGHT-->\"/>
<!--INSERT-FILECONTENT-->
    </g>"

templateRadioButton="    <g id=\"<!--INSERT-FILENAME-->\" transform=\"translate(0, 97.000000)\">
      <rect visibility=\"hidden\" id=\"BoundingRect_1_\" y=\"0\" class=\"NoFill\" width=\"<!--INSERT-WIDTH-->\" height=\"<!--INSERT-HEIGHT-->\"/>
<!--INSERT-FILECONTENT-->
    </g>"

templateScrollBar="    <g id=\"<!--INSERT-FILENAME-->\">
      <rect visibility=\"hidden\" id=\"BoundingRect_SB\" x=\"83\" y=\"3\" class=\"NoFill\" width=\"12\" height=\"5\"/>
<!--INSERT-FILECONTENT-->
    </g>"

# Volumes

templateVolume="    <g id=\"<!--INSERT-FILENAME-->\" transform=\"translate(<!--INSERT-X-->, 0.000000)\" class=\"DisplayInline\">
      <rect visibility=\"hidden\" id=\"BoundingRect_7_\" x=\"0\" y=\"0\" class=\"NoFill\" width=\"<!--INSERT-WIDTH-->\" height=\"<!--INSERT-HEIGHT-->\"/>
<!--INSERT-FILECONTENT-->
    </g>"

# Functions

templateFunctionExtended="    <g id=\"<!--INSERT-FILENAME-->\" class=\"DisplayInline\" transform=\"translate(<!--INSERT-X-->, 0.000000)\">
      <rect visibility=\"hidden\" id=\"BoundingRect_17_\" y=\"0\" class=\"NoFill\" width=\"<!--INSERT-WIDTH-->\" height=\"<!--INSERT-HEIGHT-->\"/>
<!--INSERT-FILECONTENT-->
    </g>"

templateFunction="    <g id=\"<!--INSERT-FILENAME-->\" class=\"DisplayInline\" transform=\"translate(<!--INSERT-X-->, 0.000000)\">
      <rect visibility=\"hidden\" id=\"BoundingRect_21_\" y=\"0\" class=\"NoFill\" width=\"<!--INSERT-WIDTH-->\" height=\"<!--INSERT-HEIGHT-->\"/>
<!--INSERT-FILECONTENT-->
    </g>"

# Message Row

templateMessageRow="  <g id=\"MessageRow\" class=\"DisplayBlock\" transform=\"scale(1) translate(0.000000, 580.000000)\">
    <g>
      <text id=\"MessageRowText_7_\" transform=\"matrix(1 0 0 1 <!--INSERT-X--> 25)\" class=\"FontColour FontName FontSizeMessageRow\"><!--INSERT-MESSAGE--></text>
    </g>
  </g>"

templateMessageRowNight="  <g id=\"MessageRow_night\" class=\"DisplayBlock\" transform=\"scale(1) translate(0.000000, 580.000000)\">
    <g>
      <text id=\"MessageRowText_7_\" transform=\"matrix(1 0 0 1 <!--INSERT-X--> 25)\" class=\"FontColourNight FontNameNight FontSizeMessageRow\"><!--INSERT-MESSAGE--></text>
    </g>
  </g>"

# Icons

templateSelectionIndicator="  <g id=\"<!--INSERT-FILENAME-->\"  class=\"DisplayNone\">
    <rect visibility=\"hidden\" id=\"BoundingRect_58_\" x=\"787\" y=\"452\" class=\"NoFill\" width=\"48\" height=\"48\"/>
<!--INSERT-FILECONTENT-->
  </g>"

templateSelectionIndicatorNight="  <g id=\"<!--INSERT-FILENAME-->\"  class=\"DisplayNone\">
    <rect visibility=\"hidden\" id=\"BoundingRect_58_\" x=\"787\" y=\"452\" class=\"NoFill\" width=\"48\" height=\"48\"/>
<!--INSERT-FILECONTENT-->
  </g>"

templateSelectionBig="  <g id=\"<!--INSERT-FILENAME-->\" transform=\"scale(1) translate(<!--INSERT-X-->, <!--INSERT-Y-->)\">
    <rect visibility=\"hidden\" id=\"BoundingRect_6_\" x=\"0\" y=\"0\" class=\"NoFill\" width=\"<!--INSERT-WIDTH-->\" height=\"<!--INSERT-HEIGHT-->\"/>
<!--INSERT-FILECONTENT-->
  </g>"

templateSelectionBigNight="  <g id=\"<!--INSERT-FILENAME-->\" transform=\"scale(1) translate(<!--INSERT-X-->, <!--INSERT-Y-->)\">
    <rect visibility=\"hidden\" id=\"BoundingRect_6_\" x=\"0\" y=\"0\" class=\"NoFill\" width=\"<!--INSERT-WIDTH-->\" height=\"<!--INSERT-HEIGHT-->\"/>
<!--INSERT-FILECONTENT-->
  </g>"

templateSelectionSmall="  <g id=\"<!--INSERT-FILENAME-->\" transform=\"scale(1) translate(<!--INSERT-X-->, <!--INSERT-Y-->)\" class=\"DisplayNone\">
    <rect visibility=\"hidden\" id=\"BoundingRect_16_\" y=\"0\" class=\"NoFill\" width=\"<!--INSERT-WIDTH-->\" height=\"<!--INSERT-HEIGHT-->\"/>
<!--INSERT-FILECONTENT-->
  </g>"

templateSelectionSmallNight="  <g id=\"<!--INSERT-FILENAME-->\" transform=\"scale(1) translate(<!--INSERT-X-->, <!--INSERT-Y-->)\" class=\"DisplayNone\">
    <rect visibility=\"hidden\" id=\"BoundingRect_16_\" y=\"0\" class=\"NoFill\" width=\"<!--INSERT-WIDTH-->\" height=\"<!--INSERT-HEIGHT-->\"/>
<!--INSERT-FILECONTENT-->
  </g>"

templateOSBadge="    <g id=\"<!--INSERT-FILENAME-->\" transform=\"translate(<!--INSERT-X-->, 0)\" class=\"DisplayInline\">
      <rect visibility=\"hidden\" id=\"BoundingRect_27_\" x=\"0\" y=\"0\" class=\"NoFill\" width=\"<!--INSERT-WIDTH-->\" height=\"<!--INSERT-HEIGHT-->\"/>
<!--INSERT-FILECONTENT-->
    </g>"

# Common Items

templateBanner="  <g id=\"<!--INSERT-FILENAME-->\" class=\"\">
    <rect visibility=\"hidden\" id=\"BoundingRect_44_\" y=\"0\" class=\"NoFill\" width=\"<!--INSERT-WIDTH-->\" height=\"<!--INSERT-HEIGHT-->\"/>
    <g id=\"icon_27_Banner\" transform=\"translate(<!--INSERT-X-->, <!--INSERT-Y-->)\">
<!--INSERT-FILECONTENT-->
    </g>
  </g>"

templateBannerNight="  <g id=\"<!--INSERT-FILENAME-->\" class=\"\">
    <rect visibility=\"hidden\" id=\"BoundingRect_44_\" y=\"0\" class=\"NoFill\" width=\"<!--INSERT-WIDTH-->\" height=\"<!--INSERT-HEIGHT-->\"/>
    <g id=\"icon_27_BannerNight\" transform=\"translate(<!--INSERT-X-->, <!--INSERT-Y-->)\">
<!--INSERT-FILECONTENT-->
    </g>
  </g>"

templatePointer="  <g id=\"<!--INSERT-FILENAME-->\" transform=\"translate(79.000000, 263.000000)\">
    <rect visibility=\"hidden\" id=\"BoundingRect_45_\" x=\"1\" y=\"0\" class=\"NoFill\" width=\"22\" height=\"22\"/>
<!--INSERT-FILECONTENT-->
  </g>"

# Closing Group

templateCloseGroup="</g>"

# Close Svg

templateCloseSvg="</svg>"